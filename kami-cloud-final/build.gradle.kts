group = "xyz.scootaloo"
version = "0.1"

dependencies {
    // logger
    // 高版本log4j不显示彩色日志, 需要在虚拟机选项中设置 -Dlog4j.skipJansi=false
    val log4jVersion = "2.17.1"
    implementation("org.apache.logging.log4j:log4j-api:$log4jVersion")
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:$log4jVersion")
    implementation("org.apache.logging.log4j:log4j-api:$log4jVersion")
    implementation("org.apache.logging.log4j:log4j-core:$log4jVersion")
    implementation("org.slf4j:slf4j-api:1.7.36")

    // vertx ext dependencies
    val vertxVersion = "4.2.5"
    implementation("io.vertx:vertx-web:$vertxVersion")
    implementation("io.vertx:vertx-web-openapi:$vertxVersion")
    implementation("io.vertx:vertx-auth-common:$vertxVersion")
    implementation("io.vertx:vertx-auth-jwt:$vertxVersion")

    // sqlite
    implementation("org.xerial:sqlite-jdbc:3.36.0.3")

    // ktorm ORM框架
    val ktormVersion = "3.4.1"
    implementation("org.ktorm:ktorm-core:${ktormVersion}")
    implementation("org.ktorm:ktorm-support-sqlite:${ktormVersion}")
}