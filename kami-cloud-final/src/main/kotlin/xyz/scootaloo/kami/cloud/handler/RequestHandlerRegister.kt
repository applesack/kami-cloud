@file:Suppress("unused")

package xyz.scootaloo.kami.cloud.handler

import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Router
import org.slf4j.Logger

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 14:21
 */
interface RequestHandlerRegister {

    val log: Logger

    val authMode: AuthMode get() = AuthMode.NONE

    val mountPoint: String

    fun register(router: Router)

    // Router APIs

    fun Router.get(path: String, handler: CoroutineReqHandler) = get(path).coroutineHandler(log, handler)
    fun Router.put(path: String, handler: CoroutineReqHandler) = put(path).coroutineHandler(log, handler)
    fun Router.post(path: String, handler: CoroutineReqHandler) = post(path).coroutineHandler(log, handler)
    fun Router.trace(path: String, handler: CoroutineReqHandler) = trace(path).coroutineHandler(log, handler)
    fun Router.patch(path: String, handler: CoroutineReqHandler) = patch(path).coroutineHandler(log, handler)
    fun Router.delete(path: String, handler: CoroutineReqHandler) = delete(path).coroutineHandler(log, handler)
    fun Router.options(path: String, handler: CoroutineReqHandler) = options(path).coroutineHandler(log, handler)

    fun Router.method(m: HttpMethod, path: String = "/*", handler: CoroutineReqHandler) {
        route(m, path).coroutineHandler(log, handler)
    }
}