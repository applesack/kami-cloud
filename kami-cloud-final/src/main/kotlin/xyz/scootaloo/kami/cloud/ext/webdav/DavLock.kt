package xyz.scootaloo.kami.cloud.ext.webdav

/**
 * @author flutterdash@qq.com
 * @since 2022/4/15 13:55
 */

enum class DavLockType {
    WRITE
}

enum class DavLockScope {
    EXCLUSIVE, SHARED
}

typealias DavLockOwner = String

data class DavLockRequest(
    val lockType: DavLockType,
    val lockScope: DavLockScope,
    val lockOwner: DavLockOwner
)

data class DavLockResponse(
    val lockType: DavLockType,
    val lockScope: DavLockScope,
    val lockOwner: DavLockOwner,
    val lockToken: String,
    val depth: Int,
    val timeout: Long
)

fun opaqueLockToken() {
    // todo 生成锁的标记
}

fun lockDiscovery() {
    // todo 如果有人视图去给已经被锁定的文件上锁, 则他会收到指名当前拥有者的xml元素
}

fun lockTimeout() {

}