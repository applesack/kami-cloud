package xyz.scootaloo.kami.cloud.util

import xyz.scootaloo.kami.cloud.lang.Global

/**
 * @author flutterdash@qq.com
 * @since 2022/4/15 13:11
 */
object FileResolver {

    val home: String get() = Global.APP_FILE_CONF.home
    val asset: String get() = Global.APP_FILE_CONF.assets

}