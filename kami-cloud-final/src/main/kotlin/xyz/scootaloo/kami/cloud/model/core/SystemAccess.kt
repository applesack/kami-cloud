@file:Suppress("unused")

package xyz.scootaloo.kami.cloud.model.core

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.Json
import io.vertx.kotlin.core.json.obj
import xyz.scootaloo.kami.cloud.lang.set
import xyz.scootaloo.kami.cloud.model.EntityUser

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 21:43
 */

enum class AccessType(val key: Int) {

    /**
     * 访问权限: 进入目录, 查看目录内容, 查看文件信息, 预览文件内容
     */
    ACCESS(0),

    /**
     * 下载权限: 下载文件
     */
    DOWNLOAD(1),

    /**
     * 修改权限: 创建文件, 创建目录, 上传文件
     * 修改文件内容, 移动/删除/复制文件或目录, 重命名;
     */
    MODIFY(2),

    /**
     * 定义权限: 对规则的设定和删除
     */
    DEFINE(3)

}

/**
 *
 * @author flutterdash@qq.com
 * @since 2022/2/1 11:44
 */
object AccessLevel {
    /**
     * 未登录用户具备此权限
     */
    const val GUEST = 0

    /**
     * 登录后具备此权限
     */
    const val USER = 1

    /**
     * 向超级管理员申请并通过后得到此权限
     */
    const val ADMIN = 2

    /**
     * 系统部署后, 输入正确的口令后得到此权限;
     * 一个系统中只有一个超级管理员;
     */
    const val SUPER_ADMIN = 3
}

/**
 * 定位一个用户, 当用户登录或者注册成功后, 系统会为用户签发一个jwt,
 * 其中jwt中的载体就是这个类
 *
 * @param id 用户在数据库中存储时指定的唯一ID
 * @param username 用户名
 * @param role 用户的角色, 也是用户的等级
 */
data class UserPrincipal(
    val id: Int,
    val username: String,
    val role: Int
) {
    companion object {
        fun json(entityUser: EntityUser): JsonObject {
            return Json.obj {
                this["id"] = entityUser.id
                this["username"] = entityUser.username
                this["role"] = entityUser.role
            }
        }
    }
}
