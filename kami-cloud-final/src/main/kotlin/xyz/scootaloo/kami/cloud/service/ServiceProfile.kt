package xyz.scootaloo.kami.cloud.service

import xyz.scootaloo.kami.cloud.lang.Constant
import xyz.scootaloo.kami.cloud.lang.EventLoop

/**
 * @author flutterdash@qq.com
 * @since 2022/4/11 10:02
 */
val serviceCollects = listOf(
    CacheService(), CrontabService(), AccountService()
)

interface ServiceLifeCycle {

    @EventLoop(Constant.CTX_STATE)
    fun onAppStarted() {}

    @EventLoop(Constant.CTX_STATE)
    fun onDatabaseAvailable() {}

    companion object {
        fun publishAppStartedEvent() {
            serviceCollects.filterIsInstance<ServiceLifeCycle>()
                .forEach { it.onAppStarted() }
        }

        fun publishDatabaseAvailableEvent() {
            serviceCollects.filterIsInstance<ServiceLifeCycle>()
                .forEach { it.onDatabaseAvailable() }
        }
    }
}

interface SingletonCrontab {
    fun crontab(): CrontabService.Crontab

    companion object {
        fun singletons(): List<CrontabService.Crontab> {
            return serviceCollects.filterIsInstance<SingletonCrontab>().map { it.crontab() }
        }
    }
}