package xyz.scootaloo.kami.cloud.model.dao

import org.ktorm.database.Database
import xyz.scootaloo.kami.cloud.lang.Global

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 12:26
 */
interface BaseDAO {

    val database: Database get() = Global.DATABASE_REF

}