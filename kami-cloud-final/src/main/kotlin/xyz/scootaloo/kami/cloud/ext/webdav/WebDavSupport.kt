package xyz.scootaloo.kami.cloud.ext.webdav

import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Router
import xyz.scootaloo.kami.cloud.handler.AuthMode
import xyz.scootaloo.kami.cloud.handler.RequestHandlerRegister
import xyz.scootaloo.kami.cloud.handler.remoteIpAddress
import xyz.scootaloo.kami.cloud.lang.getLogger

/**
 * [RFC4918](http://webdav.org/specs/rfc4918.html#http.methods.for.distributed.authoring)
 *
 * @author flutterdash@qq.com
 * @since 2022/4/14 19:20
 */
object WebDavSupport : RequestHandlerRegister {

    override val authMode = AuthMode.DIGEST
    override val log by lazy { getLogger("webdav-router") }
    override val mountPoint = "/dav"

    override fun register(router: Router) = router.run {
        // 获取资源和属性
        method(HttpMethod.PROPFIND) {

        }

        // 在一个或者多个资源上设定一个或多个属性
        method(HttpMethod.PROPPATCH) {

        }

        // 创建集合
        method(HttpMethod.MKCOL) {

        }

        // 从指定的源端把资源或者资源集合复制到指定的目的地, 目的地可以是另一台机器上
        method(HttpMethod.COPY) {

        }

        // 从指定的源端把资源或者资源集合移动到指定的目的地, 目的地可以是另一台机器上
        method(HttpMethod.MOVE) {

        }

        // 锁定一个或多个资源
        method(HttpMethod.LOCK) {

        }

        // 把先前锁定的资源解锁
        method(HttpMethod.UNLOCK) {

        }

        method(HttpMethod.GET) { ctx ->
            val remote = ctx.remoteIpAddress()
            ctx.end("$remote hello world!!")
        }
    }
}