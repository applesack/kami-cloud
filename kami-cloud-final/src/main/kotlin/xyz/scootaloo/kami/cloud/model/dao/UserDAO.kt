package xyz.scootaloo.kami.cloud.model.dao

import org.ktorm.dsl.eq
import org.ktorm.entity.add
import org.ktorm.entity.find
import org.ktorm.entity.take
import org.ktorm.entity.toList
import xyz.scootaloo.kami.cloud.model.EntityUser
import xyz.scootaloo.kami.cloud.model.users

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 12:25
 */
object UserDAO : BaseDAO {

    fun take(amount: Int): List<EntityUser> {
        return database.users.take(amount).toList()
    }

    fun store(user: EntityUser) {
        database.users.add(user)
    }

    fun findByName(name: String): EntityUser? {
        return database.users.find { it.username eq name }
    }
}