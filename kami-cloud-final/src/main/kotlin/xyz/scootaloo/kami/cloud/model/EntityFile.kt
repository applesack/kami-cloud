@file:Suppress("unused")

package xyz.scootaloo.kami.cloud.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.varchar

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 21:35
 */
interface EntityFile : Entity<EntityFile> {
    var id: Int
    var filepath: String
    var owner: Int
    var download: Int
    var remark: String
    var created: Long

    companion object : Entity.Factory<EntityFile>()
}

object EntityFiles : Table<EntityFile>("files") {
    val id = int("id").bindTo { it.id }.primaryKey()
    val filepath = varchar("filepath").bindTo { it.filepath }
    val owner = int("owner").bindTo { it.owner }
    val download = int("download").bindTo { it.download }
    val remark = varchar("remark").bindTo { it.remark }
    val created = long("created").bindTo { it.created }
}

val Database.files get() = sequenceOf(EntityFiles)