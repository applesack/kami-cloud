@file:Suppress("unused")

package xyz.scootaloo.kami.cloud.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.varchar

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 21:34
 */
interface EntityLog : Entity<EntityLog> {
    var id: Int
    var operator: Int
    var subject: String
    var type: String
    var desc: String
    var date: Long

    companion object : Entity.Factory<EntityLog>()
}

object EntityLogs : Table<EntityLog>("logs") {
    val id = int("id").primaryKey().bindTo { it.id }
    val operator = int("operator").bindTo { it.operator }
    val subject = varchar("subject").bindTo { it.subject }
    val type = varchar("type").bindTo { it.type }
    val desc = varchar("desc").bindTo { it.desc }
    val date = long("date").bindTo { it.date }
}

val Database.logs get() = sequenceOf(EntityLogs)