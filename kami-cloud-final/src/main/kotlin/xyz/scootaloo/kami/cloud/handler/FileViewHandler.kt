package xyz.scootaloo.kami.cloud.handler

import io.vertx.ext.web.Router
import xyz.scootaloo.kami.cloud.lang.Constant
import xyz.scootaloo.kami.cloud.lang.EventLoop
import xyz.scootaloo.kami.cloud.lang.getLogger

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 14:08
 */
@EventLoop(Constant.CTX_SERVER)
object FileViewHandler : RequestHandlerRegister {

    override val log by lazy { getLogger("file-router") }
    override val mountPoint = "/file"

    override fun register(router: Router) = router.run {
    }
}