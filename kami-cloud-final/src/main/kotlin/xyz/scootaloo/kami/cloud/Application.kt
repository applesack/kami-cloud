package xyz.scootaloo.kami.cloud

/**
 * @author flutterdash@qq.com
 * @since 2022/4/11 12:17
 */

/**
 * @property port 端口号
 * @property enableCorsHandler 是否注册跨域处理器
 * @property enableAccessInterceptor 是否启用规则拦截器(启用后, 权限不足的用户无法访问一些服务)
 * @property enableSockJs 启用后, 指定客户端(浏览器等)可以实时监听视图更新(类似websocket)
 * @property enableSlowQueryLog 启动慢查询日志, 当一条请求从被处理开始到结束, 所用时间大于[slowQueryLimit], 则该请求被记录到日志
 * @property slowQueryLimit 标记慢查询的查询时间标准(仅[enableSlowQueryLog]启动时有效)
 */
class ServerConfig(
    val port: Int = 9090,
    val enableCorsHandler: Boolean = true,
    val enableAccessInterceptor: Boolean = true,
    val enableSockJs: Boolean = true,
    val enableWebDAV: Boolean = true,
    val enableSlowQueryLog: Boolean = true,
    val slowQueryLimit: Long = 50
)

class FileConfig(
    val home: String = "./home",
    val assets: String = "./asset",
    val dbFile: String = "./conf/kami-cloud-final.sqlite"
)

class LogConfig(
    val enableHttpRecord: Boolean = true,
    val logRequestWhitResponse: Boolean = false,
    val logHttpRequestDetailWhenErrorOccurs: Boolean = true
)