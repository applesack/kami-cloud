package xyz.scootaloo.kami.cloud.util

import cn.hutool.core.util.RandomUtil

/**
 * @author flutterdash@qq.com
 * @since 2022/4/18 11:00
 */
object Random {

    fun randomString(len: Int = 8): String {
        return RandomUtil.randomString(len)
    }

}