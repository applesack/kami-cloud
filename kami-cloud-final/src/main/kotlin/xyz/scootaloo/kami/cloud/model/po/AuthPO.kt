package xyz.scootaloo.kami.cloud.model.po

import io.vertx.core.json.JsonObject
import xyz.scootaloo.kami.cloud.lang.JsonSerializable
import xyz.scootaloo.kami.cloud.lang.JsonSerializer
import kotlin.reflect.KClass

/**
 * @author flutterdash@qq.com
 * @since 2022/4/18 13:36
 */

data class DigestEncryptBlock(
    val ipAddress: String,
    val method: String,
    val authHeader: String?
) : JsonSerializable

object DigestEncryptBlockSerializer :  JsonSerializer<DigestEncryptBlock> {
    override fun accept(): KClass<DigestEncryptBlock> {
        return DigestEncryptBlock::class
    }

    override fun deserialize(json: JsonObject): DigestEncryptBlock {
        return DigestEncryptBlock(
            ipAddress = json.getString("ipAddress")!!,
            method = json.getString("method")!!,
            authHeader = json.getString("authHeader")
        )
    }
}