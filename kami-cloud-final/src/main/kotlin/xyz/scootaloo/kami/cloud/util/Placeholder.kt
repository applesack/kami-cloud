package xyz.scootaloo.kami.cloud.util

import xyz.scootaloo.kami.cloud.lang.Stateless

/**
 * @author flutterdash@qq.com
 * @since 2022/4/11 21:14
 */
@Stateless
object Placeholder {

    fun splice(pattern: String, vararg appender: Any): String {
        val buff = StringBuilder()
        var pos = 0
        var appenderPos = 0
        while (pos < pattern.length) {
            val ch = pattern[pos]
            if (ch == '{' && pos < (pattern.length - 1) && pattern[pos + 1] == '}') {
                if (appenderPos < appender.size) {
                    buff.append(appender[appenderPos++])
                }
                pos += 2
            } else {
                buff.append(ch)
                pos++
            }
        }

        return buff.toString()
    }

}