package xyz.scootaloo.kami.cloud.model.po

import io.vertx.core.json.JsonObject
import xyz.scootaloo.kami.cloud.lang.JsonSerializable
import xyz.scootaloo.kami.cloud.lang.JsonSerializer
import kotlin.reflect.KClass

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 10:40
 */
data class CacheEntry(
    val key: String,
    val value: String,
    val expiryTime: Long
) : JsonSerializable

object CacheEntrySerializer : JsonSerializer<CacheEntry> {
    override fun accept(): KClass<CacheEntry> {
        return CacheEntry::class
    }

    override fun deserialize(json: JsonObject): CacheEntry {
        return CacheEntry(
            key = json.getString("key")!!,
            value = json.getString("value")!!,
            expiryTime = json.getLong("expiryTime")!!
        )
    }
}

data class CacheUpdateExpiry(
    val key: String,
    val newExpiryTime: Long
) : JsonSerializable

object CacheUpdateExpirySerializer : JsonSerializer<CacheUpdateExpiry> {
    override fun accept(): KClass<CacheUpdateExpiry> {
        return CacheUpdateExpiry::class
    }

    override fun deserialize(json: JsonObject): CacheUpdateExpiry {
        return CacheUpdateExpiry(
            key = json.getString("key")!!,
            newExpiryTime = json.getLong("newExpiryTime")!!
        )
    }
}