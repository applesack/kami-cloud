package xyz.scootaloo.kami.cloud.lang

import kotlin.annotation.AnnotationTarget.*
import kotlin.reflect.KClass

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 15:01
 */

/**
 * 标记一个方法或者类, 其内容应该运行在事件循环上下文中
 */
@Target(CLASS, FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class EventLoop(val mark: String)

/**
 * 标记一个方法会在multi-worker上下文调用,
 * 设计api时, 应该考虑到并发问题
 */
@Target(CLASS, FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class MultiWorker

/**
 * 标记一个类或者方法, 可以在任意位置访问(不限制上下文环境)
 */
@Target(CLASS, FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class Public

/**
 * 标记一个类是无状态的
 */
@Target(CLASS, FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class Stateless

/**
 * 标记一个类或者方法, 只允许被eventbus中的consumer调用
 *
 * @see Bus
 * @property address api总线地址
 * @property param 参数类型
 */
@Target(CLASS, FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class EventbusConsumerApi(val address: String, val param: KClass<*> = Unit::class)

/**
 * 标记一个方法不能通过外部直接调用, 只能由系统内部使用;
 * 或者标记一个类不能直接被使用, 其方法由事件触发调用
 */
@Target(CLASS, FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class InternalApi