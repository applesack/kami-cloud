package xyz.scootaloo.kami.cloud.model.po

import io.vertx.core.json.JsonObject
import xyz.scootaloo.kami.cloud.lang.JsonSerializable
import xyz.scootaloo.kami.cloud.lang.JsonSerializer
import kotlin.reflect.KClass

/**
 * @author flutterdash@qq.com
 * @since 2022/4/17 12:40
 */

data class CEntry(
    val key: String,
    val value: Any
) : JsonSerializable

object CEntrySerializer : JsonSerializer<CEntry> {
    override fun accept(): KClass<CEntry> {
        return CEntry::class
    }

    override fun deserialize(json: JsonObject): CEntry {
        return CEntry(
            key = json.getString("key")!!,
            value = json.getValue("value")!!
        )
    }
}