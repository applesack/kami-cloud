@file:Suppress("unused")

package xyz.scootaloo.kami.cloud.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.varchar

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 21:35
 */
interface EntityLimit : Entity<EntityLimit> {

    var id: Int
    var created: Long
    var updated: Long
    var path: String
    var capacity: Long

    companion object : Entity.Factory<EntityLimit>()
}

object EntityLimits : Table<EntityLimit>("limits") {

    val id = int("id").bindTo { it.id }.primaryKey()
    val created = long("created").bindTo { it.created }
    val updated = long("updated").bindTo { it.updated }
    val path = varchar("path").bindTo { it.path }
    val capacity = long("capacity").bindTo { it.capacity }

}

val Database.limits get() = this.sequenceOf(EntityLimits)