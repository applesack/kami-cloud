package xyz.scootaloo.kami.cloud.util

import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 9:06
 */

fun Future<*>.toUnit(): Future<Unit> {
    return compose { succeededFuture(Unit) }
}

fun <T> Future<T?>.notNull(): Future<T> {
    return compose { succeededFuture(it!!) }
}

fun <T> T.wrapFut(): Future<T> {
    return succeededFuture(this)
}