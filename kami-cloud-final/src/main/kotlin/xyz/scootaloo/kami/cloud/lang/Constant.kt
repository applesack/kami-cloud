package xyz.scootaloo.kami.cloud.lang

import xyz.scootaloo.kami.cloud.verticle.HttpServerVerticle
import xyz.scootaloo.kami.cloud.verticle.StateCenterVerticle

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 15:12
 */
object Constant {

    const val OK = "OK"

    const val KEY_QUERY_BEGIN = "query_begin"
    const val KEY_QUERY_FINISH = "query_finish"
    const val KEY_HTTP_PROC_ERR = "router_proc_err"
    const val KEY_HTTP_RESP_STORE = "resp_store"
    const val KEY_USER_PRINCIPAL = "user-principal"

    /**
     * 状态管理上下文, 在此上下文中可以访问内部缓存等服务, 参考 [StateCenterVerticle]
     */
    const val CTX_STATE = "state-manager-EventLoop-context"

    /**
     * http服务器上下文, 此上下文管理的状态参考 [HttpServerVerticle]
     */
    const val CTX_SERVER = "http-server-EventLoop-context"

    const val EB_PLACEHOLDER = "&"

    const val EB_CACHE_PREFIX = "sys.cache"
    const val EB_CACHE_GET = "$EB_CACHE_PREFIX#get"
    const val EB_CACHE_PUT = "$EB_CACHE_PREFIX#put"
    const val EB_CACHE_DEL = "$EB_CACHE_PREFIX#del"
    const val EB_CACHE_UpEx = "$EB_CACHE_PREFIX#update_expiry"

    const val EB_ACCOUNT_PREFIX = "sys.account"
    const val EB_ACCOUNT_REGISTER = "$EB_ACCOUNT_PREFIX#register"
    const val EB_ACCOUNT_LOGIN = "$EB_ACCOUNT_PREFIX#login"
    const val EB_ACCOUNT_P_LOGIN = "$EB_ACCOUNT_PREFIX#plaintext_login"
    const val EB_ACCOUNT_LOGOUT = "$EB_ACCOUNT_PREFIX#logout"
    const val EB_ACCOUNT_FD_USER = "$EB_ACCOUNT_PREFIX#find_user"

    const val EB_TOKEN_PREFIX = "sys.token"
    const val EB_TOKEN_GEN = "$EB_TOKEN_PREFIX#generateToken"
    const val EB_TOKEN_DEC = "$EB_TOKEN_PREFIX#decodeContent"
}