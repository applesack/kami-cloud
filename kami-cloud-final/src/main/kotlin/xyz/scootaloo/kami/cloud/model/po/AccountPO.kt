package xyz.scootaloo.kami.cloud.model.po

import io.vertx.core.json.JsonObject
import xyz.scootaloo.kami.cloud.lang.JsonSerializable
import xyz.scootaloo.kami.cloud.lang.JsonSerializer
import xyz.scootaloo.kami.cloud.lang.deserializer
import kotlin.reflect.KClass

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 9:30
 */
data class AccountForm(
    val username: String,
    val password: String,
    val clientId: String,
    val token: String?
) : JsonSerializable

object AccountFormSerializer : JsonSerializer<AccountForm> {
    override fun accept(): KClass<AccountForm> {
        return AccountForm::class
    }

    override fun deserialize(json: JsonObject): AccountForm {
        return AccountForm(
            username = json.getString("username")!!,
            password = json.getString("password")!!,
            clientId = json.getString("clientId")!!,
            token = json.getString("token")
        )
    }
}

data class AccountFormBlock(
    val ipAddress: String,
    val form: AccountForm
) : JsonSerializable

object AccountFormBlockSerializer : JsonSerializer<AccountFormBlock> {
    override fun accept(): KClass<AccountFormBlock> {
        return AccountFormBlock::class
    }

    override fun deserialize(json: JsonObject): AccountFormBlock {
        return AccountFormBlock(
            ipAddress = json.getString("ipAddress")!!,
            form = deserializer(json.getJsonObject("form"))
        )
    }
}

data class AccountPlaintextFormBlock(
    val ipAddress: String,
    val username: String,
    val password: String
) : JsonSerializable

object AccountPlaintextFormBlockSerializer : JsonSerializer<AccountPlaintextFormBlock> {
    override fun accept(): KClass<AccountPlaintextFormBlock> {
        return AccountPlaintextFormBlock::class
    }

    override fun deserialize(json: JsonObject): AccountPlaintextFormBlock {
        return AccountPlaintextFormBlock(
            ipAddress = json.getString("ipAddress")!!,
            username = json.getString("password")!!,
            password = json.getString("password")!!
        )
    }
}