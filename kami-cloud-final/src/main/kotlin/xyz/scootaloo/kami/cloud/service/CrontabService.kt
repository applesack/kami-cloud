package xyz.scootaloo.kami.cloud.service

import xyz.scootaloo.kami.cloud.lang.Constant
import xyz.scootaloo.kami.cloud.lang.EventLoop
import xyz.scootaloo.kami.cloud.lang.InternalApi
import xyz.scootaloo.kami.cloud.model.EntityTask
import xyz.scootaloo.kami.cloud.service.impl.CrontabServiceImpl

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 21:23
 */
@InternalApi
interface CrontabService {

    @InternalApi
    fun submit(crontab: Crontab)

    interface Crontab {
        /**
         * 可以在[run]方法中修改这个属性, 当这个属性为false时, 当前任务会从全局任务中注销
         */
        var valid: Boolean

        /**
         * 任务名, 这个名称必须是唯一的
         */
        val name: String

        /**
         * 执行任务的间隔(单位毫秒), 可以动态调整
         */
        var delay: Long

        /**
         * 优先级, 数字越小优先级越高; 只能取1 - 9, 包括1和9
         */
        val order: Int

        /**
         * 定时执行的任务, 这个方法中的代码会EventLoop上下文中执行, 所以尽量避免让线程阻塞
         */
        @EventLoop(Constant.CTX_STATE)
        fun run(currentTimeMillis: Long)

        companion object {
            const val defOrder = 5
            const val defDelay = 100L
        }
    }

    interface DatabaseCrontabHandler {
        fun handle(dbTask: EntityTask)

        companion object {
            const val UPLOAD_TASK = 1
        }
    }

    companion object {
        operator fun invoke(): CrontabService {
            return CrontabServiceImpl
        }
    }

}