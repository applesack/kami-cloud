@file:Suppress("unused")

package xyz.scootaloo.kami.cloud.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.*

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 21:35
 */
interface EntityRule : Entity<EntityRule> {
    var id: Int
    var created: Long
    var updated: Long
    var path: String
    var accessRule: ByteArray

    companion object : Entity.Factory<EntityRule>()
}

object EntityRules : Table<EntityRule>("rules") {
    val id = int("id").bindTo { it.id }.primaryKey()
    val created = long("created").bindTo { it.created }
    val updated = long("updated").bindTo { it.updated }
    val path = varchar("path").bindTo { it.path }
    val accessRule = bytes("access_rule").bindTo { it.accessRule }
}

val Database.rules get() = this.sequenceOf(EntityRules)