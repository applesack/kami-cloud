package xyz.scootaloo.kami.cloud.service

import io.vertx.core.Future
import xyz.scootaloo.kami.cloud.lang.Constant
import xyz.scootaloo.kami.cloud.lang.EventbusConsumerApi
import xyz.scootaloo.kami.cloud.model.EntityUser
import xyz.scootaloo.kami.cloud.model.po.AccountForm
import xyz.scootaloo.kami.cloud.model.po.AccountFormBlock
import xyz.scootaloo.kami.cloud.model.po.ServiceResult
import xyz.scootaloo.kami.cloud.service.impl.AccountServiceImpl

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 9:23
 */
interface AccountService {

    fun register(ipAddress: String, form: AccountForm): Future<ServiceResult<String>>
    fun login(ipAddress: String, form: AccountForm): Future<ServiceResult<String>>
    fun login(ipAddress: String, username: String, password: String): Future<ServiceResult<Unit>>
    fun logout(): Future<ServiceResult<Unit>>
    fun findUser(username: String): Future<EntityUser?>
    fun internal(): InternalAccountServiceApi

    @EventbusConsumerApi(Constant.EB_ACCOUNT_PREFIX)
    interface InternalAccountServiceApi {
        @EventbusConsumerApi(Constant.EB_ACCOUNT_REGISTER, AccountFormBlock::class)
        fun register(ipAddress: String, form: AccountForm): Future<ServiceResult<String>>

        @EventbusConsumerApi(Constant.EB_ACCOUNT_LOGIN, AccountFormBlock::class)
        fun login(ipAddress: String, form: AccountForm): Future<ServiceResult<String>>

        @EventbusConsumerApi(Constant.EB_ACCOUNT_P_LOGIN)
        fun login(ipAddress: String, username: String, password: String): Future<ServiceResult<Unit>>

        @EventbusConsumerApi(Constant.EB_ACCOUNT_LOGOUT)
        fun logout(): Future<ServiceResult<Unit>>

        @EventbusConsumerApi(Constant.EB_ACCOUNT_FD_USER, String::class)
        fun findUserByName(username: String): Future<EntityUser?>
    }

    companion object {
        operator fun invoke(): AccountService {
            return AccountServiceImpl
        }
    }
}