package xyz.scootaloo.kami.cloud.lang

import io.vertx.core.Vertx
import io.vertx.core.eventbus.EventBus
import io.vertx.core.file.FileSystem
import org.ktorm.database.Database
import xyz.scootaloo.kami.cloud.LogConfig
import xyz.scootaloo.kami.cloud.FileConfig
import xyz.scootaloo.kami.cloud.ServerConfig

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 15:26
 */
object Global {

    lateinit var VERTX_REF: Vertx
    lateinit var DATABASE_REF: Database

    // 发布后不可修改

    lateinit var APP_SER_CONF: ServerConfig
    lateinit var APP_FILE_CONF: FileConfig
    lateinit var APP_LOG_CONF: LogConfig

    fun eventbus(): EventBus {
        return VERTX_REF.eventBus()
    }

    fun filesystem(): FileSystem {
        return VERTX_REF.fileSystem()
    }
}