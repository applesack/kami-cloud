package xyz.scootaloo.kami.cloud.service

import io.vertx.core.Future
import xyz.scootaloo.kami.cloud.lang.Constant
import xyz.scootaloo.kami.cloud.lang.EventbusConsumerApi
import xyz.scootaloo.kami.cloud.model.po.EncodeToken
import xyz.scootaloo.kami.cloud.model.po.TokenGenBlock
import xyz.scootaloo.kami.cloud.model.po.ServiceResult
import xyz.scootaloo.kami.cloud.service.impl.TokenServiceImpl

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 16:05
 */
interface TokenService {

    fun generateToken(ipAddress: String, expiryTime: Long = CacheService.DEF_EXPIRY_TIME): Future<EncodeToken>

    fun decodeContent(ipAddress: String, clientId: String, base64encoded: String): Future<ServiceResult<String>>

    fun internal(): InternalTokenServiceApi

    @EventbusConsumerApi(Constant.EB_TOKEN_PREFIX)
    interface InternalTokenServiceApi {
        fun link(): TokenService

        @EventbusConsumerApi(Constant.EB_TOKEN_GEN, TokenGenBlock::class)
        fun generateToken(ipAddress: String, expiryTime: Long = CacheService.DEF_EXPIRY_TIME): EncodeToken

        @EventbusConsumerApi(Constant.EB_TOKEN_DEC)
        fun decodeContent(ipAddress: String, clientId: String, base64encoded: String): ServiceResult<String>
    }

    companion object {
        operator fun invoke(): TokenService {
            return TokenServiceImpl
        }
    }
}