package xyz.scootaloo.kami.cloud.model.po

import io.vertx.core.json.JsonObject
import xyz.scootaloo.kami.cloud.lang.JsonSerializable
import xyz.scootaloo.kami.cloud.lang.JsonSerializer
import kotlin.reflect.KClass

/**
 * @author flutterdash@qq.com
 * @since 2022/4/12 16:08
 */

data class EncodeToken(
    val clientId: String,
    val publicKey: String
) : JsonSerializable

object EncodeTokenSerializer : JsonSerializer<EncodeToken> {
    override fun accept(): KClass<EncodeToken> {
        return EncodeToken::class
    }

    override fun deserialize(json: JsonObject): EncodeToken {
        return EncodeToken(
            clientId = json.getString("clientId")!!,
            publicKey = json.getString("publicKey")!!
        )
    }
}

class TokenGenBlock(
    val ipAddress: String,
    val expiryTime: Long
) : JsonSerializable

object TokenGenBlockSerializer : JsonSerializer<TokenGenBlock> {
    override fun accept(): KClass<TokenGenBlock> {
        return TokenGenBlock::class
    }

    override fun deserialize(json: JsonObject): TokenGenBlock {
        return TokenGenBlock(
            ipAddress = json.getString("ipAddress")!!,
            expiryTime = json.getLong("expiryTime")!!
        )
    }
}

class TokenContentDecodeBlock(
    val ipAddress: String,
    val clientId: String,
    val base64encoded: String
) : JsonSerializable

object ContentDecodeBlockSerializer : JsonSerializer<TokenContentDecodeBlock> {
    override fun accept(): KClass<TokenContentDecodeBlock> {
        return TokenContentDecodeBlock::class
    }

    override fun deserialize(json: JsonObject): TokenContentDecodeBlock {
        return TokenContentDecodeBlock(
            ipAddress = json.getString("ipAddress")!!,
            clientId = json.getString("clientId")!!,
            base64encoded = json.getString("base64encoded")!!
        )
    }
}