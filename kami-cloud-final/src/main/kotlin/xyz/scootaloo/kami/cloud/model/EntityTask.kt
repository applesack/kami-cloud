@file:Suppress("unused")

package xyz.scootaloo.kami.cloud.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.varchar

/**
 * @author flutterdash@qq.com
 * @since 2022/4/10 21:32
 */
interface EntityTask : Entity<EntityTask> {

    var id: Int
    var type: Int
    var user: Int
    var created: Long
    var state: Int
    var payload: String

    companion object : Entity.Factory<EntityTask>()
}

object EntityTasks : Table<EntityTask>("tasks") {

    val id = int("id").bindTo { it.id }.primaryKey()
    val type = int("type").bindTo { it.type }
    val user = int("user").bindTo { it.user }
    val state = int("state").bindTo { it.state }
    val created = long("created").bindTo { it.created }
    val payload = varchar("payload").bindTo { it.payload }

}

val Database.tasks get() = sequenceOf(EntityTasks)