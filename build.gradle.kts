plugins {
    application
    kotlin("jvm") version "1.6.10"
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

allprojects {
    group = "xyz.scootaloo"
    version = "0.1"

    repositories {
        maven("https://maven.aliyun.com/repository/public/")
        mavenLocal()
        mavenCentral()
    }

    tasks.withType<Test>() {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }
    }
}

subprojects {
    apply {
        plugin("application")
        plugin("org.jetbrains.kotlin.jvm")
        plugin("com.github.johnrengelman.shadow")
    }

    dependencies {
        implementation(kotlin("stdlib"))

        implementation("cn.hutool:hutool-all:5.7.18")

        // vertx dependencies
        val vertxVersion = "4.2.5"
        implementation("io.vertx:vertx-core:$vertxVersion")
        implementation("io.vertx:vertx-config:$vertxVersion")
        implementation("io.vertx:vertx-lang-kotlin:$vertxVersion")
        implementation("io.vertx:vertx-lang-kotlin-coroutines:$vertxVersion")

        // kotlin reflection utils
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
        implementation("org.jetbrains.kotlin:kotlin-reflect")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")

        // test dependencies
        testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
        testImplementation("org.junit.jupiter:junit-jupiter-engine:5.8.2")
    }
}