import {Component, Input, OnInit} from '@angular/core';
import {AppContextService} from "../../service/app-context.service";
import {FileViewService} from "../../service/file-view.service";
import {FileDesc} from "../../model/file-view";
import {FileModifyService} from "../../service/file-modify.service";
import {NzMessageService} from "ng-zorro-antd/message";

@Component({
  selector: 'app-file-context-menu',
  templateUrl: './file-context-menu.component.html',
  styleUrls: ['./file-context-menu.component.css']
})
export class FileContextMenuComponent implements OnInit {

  drawerVisible = false
  drawerTitle = ''
  drawerLoading = false

  file: FileDesc | undefined

  constructor(
    private ctx: AppContextService,
    private viewService: FileViewService,
    private fileModifyService: FileModifyService,
    private message: NzMessageService
  ) {
  }

  ngOnInit(): void {
  }

  isDirectory(): boolean {
    const file = this.ctx.currentFile
    if (file == undefined)
      return false
    return this.viewService.isDirectory(file)
  }

  isOpenable(): boolean {
    const file = this.ctx.currentFile
    if (file == undefined)
      return false
    return this.viewService.isOpenable(file)
  }

  isEditable(): boolean {
    const file = this.ctx.currentFile
    if (file == undefined)
      return false
    return this.viewService.isEditable(file)
  }

  isZip(): boolean {
    const file = this.ctx.currentFile
    if (file == undefined)
      return false
    return this.viewService.isZipFile(file)
  }

  hasSelectedFile(): boolean {
    return this.ctx.currentFile != undefined
  }

  openCreateFileModal() {

  }

  openCreateDirModal() {

  }

  doCreate() {

  }

  createFile() {
  }

  renameFile() {

  }

  deleteFile() {
    const file = this.ctx.currentFile
    if (file == undefined)
      return
    const ref = this
    this.fileModifyService.deleteFile(file).subscribe({
      next(r) {
        if (!r.success) {
          console.log('内部错误')
          ref.message.error(r.message)
        }
      },
      error(e: any) {
        console.log(e)
        ref.message.error('网络错误')
      }
    })
  }

  showFileDetails() {
    this.drawerVisible = true
    this.drawerLoading = true
    this.viewService.displayFileDetails(this.ctx.currentFile!).subscribe({
      next(r) {

      },
      error(err) {
      },
      complete() {
      }
    })
  }
}
