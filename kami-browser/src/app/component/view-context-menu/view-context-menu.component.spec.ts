import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewContextMenuComponent } from './view-context-menu.component';

describe('ViewContextMenuComponent', () => {
  let component: ViewContextMenuComponent;
  let fixture: ComponentFixture<ViewContextMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewContextMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
