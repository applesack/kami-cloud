import { TestBed } from '@angular/core/testing';

import { EncryptService } from './encrypt.service';

describe('EncrypService', () => {
  let service: EncryptService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EncryptService);
  });

  it('should be created', (fn) => {
    expect(service).toBeTruthy();
  });
});
