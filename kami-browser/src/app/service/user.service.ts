import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  hasLogin = false

  constructor() {
  }

  login(jwt: string) {
    this.hasLogin = true
    this.updateJwt(jwt)
  }

  logout() {
    localStorage.clear()
  }

  getJwt(): string | null {
    return localStorage.getItem("jwt")
  }

  updateJwt(jwt: string | null) {
    if (jwt == null) {
      localStorage.removeItem("jwt")
    } else {
      localStorage.setItem("jwt", jwt)
    }
  }

}
