import {APP_INITIALIZER, Injectable} from '@angular/core';
import {ThemeService} from "./theme.service";

export const AppInitializerProvider = {
  provide: APP_INITIALIZER,
  useFactory: (themeService: ThemeService) => () => {
    return themeService.loadTheme()
  },
  deps: [ThemeService],
  multi: true
}

@Injectable({
  providedIn: 'root'
})
export class AppInitializerService {
}
