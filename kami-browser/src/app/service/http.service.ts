import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor() { }

  baseUrl(): string {
    if (environment.production) {
      return HttpService.serverHost(9090, true)
    } else {
      return HttpService.serverHost(9090, false)
    }
  }

  private static serverHost(port: number, prod: boolean): string {
    const url = window.document.location.href
    const idx = url.indexOf(':', 5)
    if (prod) {
      const splitter = url.indexOf('/', idx)
      return url.substring(0, splitter)
    } else {
      return url.substring(0, idx) + ':' + port
    }
  }

}
