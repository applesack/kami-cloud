import { TestBed } from '@angular/core/testing';

import { FileModifyService } from './file-modify.service';

describe('FileModifyService', () => {
  let service: FileModifyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileModifyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
