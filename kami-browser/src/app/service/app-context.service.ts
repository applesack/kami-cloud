import {Injectable} from '@angular/core';
import {FileDesc} from "../model/file-view";
import {Subject} from "rxjs";

export interface AppMenuItem {
  name: string
}

@Injectable({
  providedIn: 'root'
})
export class AppContextService {

  private _currentPath?: string

  currentFiles?: FileDesc[]

  sideMenus = []

  doSelectUploadFiles = new Subject<boolean>()

  private _currentFile?: FileDesc

  enableMultipleChoice = false
  enableTopMenu = false

  constructor() {

  }

  get currentFile(): FileDesc | undefined {
    return this._currentFile
  }

  set currentFile(file: FileDesc | undefined) {
    if (file === undefined) { // 将上一次选中的文件清空
      if (this._currentFile !== undefined) {
        this._currentFile.selected = false
        this._currentFile = undefined
      }
    } else { // 将上一次选中的文件取消选中, 然后选中当前文件
      if (this._currentFile !== undefined) {
        this._currentFile.selected = false
      }
      file.selected = true
      this._currentFile = file
    }
  }

  get currentPath(): string | undefined {
    return this._currentPath
  }

  set currentPath(path: string | undefined) {
    this._currentPath = path
  }

  toggleMultiChoice() {
    this.enableMultipleChoice = !this.enableMultipleChoice
  }

  quitTopMenu() {
    this.enableTopMenu = false
    this.enableMultipleChoice = false
    this.currentFile = undefined
  }

  justQuitTopMenu() {
    this.enableTopMenu = false
  }

  openTopMenu() {
    this.enableTopMenu = true
  }

  isCurrentFile(file: FileDesc): boolean {
    return this._currentFile === file
  }

  doSelectedFile(file: FileDesc) {
    this.currentFile = file
    this.openTopMenu()
  }

  justSelectedFile(file: FileDesc) {
    this.currentFile = file
  }

  doUploadFile(isFile: boolean) {
    this.doSelectUploadFiles.next(isFile)
  }

  release() {
    if (this.currentFile != undefined) {
      this.currentFile.selected = false
      this.currentFile = undefined
    }
    this.quitTopMenu()
  }
}

