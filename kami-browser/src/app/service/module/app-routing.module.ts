import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from "../../mainpage/home-page/home-page.component";
import {MyFileComponent} from "../../subpage/my-file/my-file.component";
import {MyVideosComponent} from "../../subpage/my-videos/my-videos.component";
import {MyAudioComponent} from "../../subpage/my-audio/my-audio.component";
import {MyPictureComponent} from "../../subpage/my-picture/my-picture.component";
import {MyDocumentComponent} from "../../subpage/my-document/my-document.component";
import {LoginPageComponent} from "../../mainpage/login-page/login-page.component";
import {RegisterPageComponent} from "../../mainpage/register-page/register-page.component";
import {UploadTestComponent} from "../../subpage/upload-test/upload-test.component";

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'login', component: LoginPageComponent, pathMatch: 'full'},
  {path: 'register', component: RegisterPageComponent, pathMatch: 'full'},
  {
    path: 'home', component: HomePageComponent, children: [
      {path: '', redirectTo: '/home/file', pathMatch: 'full'},
      {path: 'file', component: MyFileComponent, pathMatch: 'prefix'},
      {path: 'videos', component: MyVideosComponent},
      {path: 'audio', component: MyAudioComponent},
      {path: 'picture', component: MyPictureComponent},
      {path: 'document', component: MyDocumentComponent},
      {path: 'upload', component: UploadTestComponent}
    ]
  }
]


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
