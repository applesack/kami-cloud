import { TestBed } from '@angular/core/testing';

import { HttpService } from './http.service';
import {main} from "@angular/compiler-cli/src/main";

describe('HttpService', () => {
  let service: HttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be true', function () {
    const text = '你好世界! hello world!'
    const encoded = window.btoa(text)
    console.log(`加密${encoded}`)
    const decoded = window.atob(encoded)
    console.log(`加密${decoded}`)
    expect(service).toBeTruthy();
  });
});
