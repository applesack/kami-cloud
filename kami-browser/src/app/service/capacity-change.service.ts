import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {CapacityChangeEvent} from "../model/broadcast-dto";

@Injectable({
  providedIn: 'root'
})
export class CapacityChangeService {

  change: Subject<CapacityChangeEvent> = new Subject<CapacityChangeEvent>()

  constructor() { }
}
