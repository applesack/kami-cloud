import {Injectable} from '@angular/core';
import {JSEncrypt} from "jsencrypt";

@Injectable({
  providedIn: 'root'
})
export class EncryptService {

  constructor() { }

  encodeByPublicKey(context: string, publicKey: string): string {
    const encryptor = new JSEncrypt()
    encryptor.setPublicKey(publicKey)
    return encryptor.encrypt(context) as string
  }

  base64decode(content: string): string {
    return window.atob(content)
  }

}
