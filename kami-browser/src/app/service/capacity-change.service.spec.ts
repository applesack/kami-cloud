import { TestBed } from '@angular/core/testing';

import { CapacityChangeService } from './capacity-change.service';

describe('CapacityChangeService', () => {
  let service: CapacityChangeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CapacityChangeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
