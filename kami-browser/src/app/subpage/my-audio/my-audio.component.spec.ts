import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAudioComponent } from './my-audio.component';

describe('MyAudioComponent', () => {
  let component: MyAudioComponent;
  let fixture: ComponentFixture<MyAudioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyAudioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
