import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './service/module/app-routing.module';
import {UploadComponent} from './component/upload/upload.component';
import {NZ_I18N, zh_CN} from 'ng-zorro-antd/i18n';
import {registerLocaleData} from '@angular/common';
import zh from '@angular/common/locales/zh';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzMessageService} from "ng-zorro-antd/message";
import {AppInitializerProvider} from "./service/app-initializer.service";
import {HomePageComponent} from './mainpage/home-page/home-page.component';
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {MyFileComponent} from './subpage/my-file/my-file.component';
import {MyVideosComponent} from './subpage/my-videos/my-videos.component';
import {MyAudioComponent} from './subpage/my-audio/my-audio.component';
import {MyPictureComponent} from './subpage/my-picture/my-picture.component';
import {MyDocumentComponent} from './subpage/my-document/my-document.component';
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {BreadcrumbModule} from "ng-devui";
import {XCrumbModule} from "@ng-nest/ui";
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {IconsProviderModule} from "./service/module/icons-provider.module";
import {NzSpaceModule} from "ng-zorro-antd/space";
import { LoginPageComponent } from './mainpage/login-page/login-page.component';
import {NzFormModule} from "ng-zorro-antd/form";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import { LoginBoxComponent } from './component/login-box/login-box.component';
import { RegisterPageComponent } from './mainpage/register-page/register-page.component';
import {JwtInterceptor} from "./interceptor/jwt.interceptor";
import { UploadTestComponent } from './subpage/upload-test/upload-test.component';
import {NzProgressModule} from "ng-zorro-antd/progress";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzDescriptionsModule} from "ng-zorro-antd/descriptions";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {NzNotificationService} from "ng-zorro-antd/notification";
import { FileContextMenuComponent } from './component/file-context-menu/file-context-menu.component';
import { ViewContextMenuComponent } from './component/view-context-menu/view-context-menu.component';
import {NzDrawerModule} from "ng-zorro-antd/drawer";
import {NzSkeletonModule} from "ng-zorro-antd/skeleton";

registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,
    UploadComponent,
    HomePageComponent,
    LoginPageComponent,
    MyFileComponent,
    MyVideosComponent,
    MyAudioComponent,
    MyPictureComponent,
    MyDocumentComponent,
    LoginBoxComponent,
    RegisterPageComponent,
    UploadTestComponent,
    FileContextMenuComponent,
    ViewContextMenuComponent,
  ],
  imports: [
    IconsProviderModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzUploadModule,
    NzButtonModule,
    NzIconModule,
    NzLayoutModule,
    NzMenuModule,
    NzAvatarModule,
    NzInputModule,
    NzBreadCrumbModule,
    NzDropDownModule,
    BreadcrumbModule,
    XCrumbModule,
    NzPageHeaderModule,
    NzRadioModule,
    NzSelectModule,
    NzTableModule,
    NzDividerModule,
    BrowserAnimationsModule,
    NzSpaceModule,
    NzFormModule,
    ReactiveFormsModule,
    NzCheckboxModule,
    NzProgressModule,
    NzModalModule,
    NzDescriptionsModule,
    NzTagModule,
    NzTypographyModule,
    NzDrawerModule,
    NzSkeletonModule
  ],
  providers: [
    AppInitializerProvider,
    { provide: NZ_I18N, useValue: zh_CN },
    { provide: NzMessageService },
    { provide: NzNotificationService },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
