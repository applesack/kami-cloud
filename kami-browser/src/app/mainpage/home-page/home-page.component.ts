import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ThemeService} from 'src/app/service/theme.service';
import {NzIconService} from "ng-zorro-antd/icon";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {AppContextService} from "../../service/app-context.service";
import {FileViewService} from "../../service/file-view.service";
import {NzButtonComponent} from "ng-zorro-antd/button";
import {NzProgressStatusType} from "ng-zorro-antd/progress/typings";
import {SockJsService} from "../../service/sock-js.service";
import {CapacityChangeService} from "../../service/capacity-change.service";
import {PathService} from "../../service/path.service";
import {CapacityChangeEvent} from "../../model/broadcast-dto";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.less'],
  animations: [
    trigger('menuOpenClose', [
      state('open', style({
        top: '0px'
      })),
      state('closed', style({
        top: '-64px'
      })),
      transition('open => closed', [
        animate('0.2s')
      ]),
      transition('closed => open', [
        animate('0.2s')
      ])
    ])
  ]
})
export class HomePageComponent implements OnInit {

  isCollapsed = false;

  // 基路径的容量状态
  percent = 0
  state: NzProgressStatusType = 'success'
  total = 1
  used = 0

  constructor(
    private themeService: ThemeService,
    private iconService: NzIconService,
    public viewService: FileViewService,
    public ctx: AppContextService,
    public sockJsService: SockJsService,
    private pathService: PathService,
    private capChangeService: CapacityChangeService
  ) {
    const ref = this
    this.iconService.fetchFromIconfont({
      scriptUrl: 'https://at.alicdn.com/t/font_3166107_0k2pic87p1qu.js'
    })
    this.sockJsService.initSockJs()
    this.capChangeService.change.subscribe({
      next(value: CapacityChangeEvent) {
        console.log(value)
        if (pathService.matchPath(value.path as string, '')) {
          ref.updateSpaceProgress(value.limit, value.free)
        } else {
          console.log('匹配失败')
        }
      }
    })
  }

  ngOnInit(): void {
    this.uploadServerStorageSpaceProgress()
  }

  uploadServerStorageSpaceProgress() {
    const ref = this
    this.viewService.displayServerStorageSpace()
      .subscribe({
        next(r) {
          const cap = r.data!!
          ref.updateSpaceProgress(cap.limit, cap.free)
        },
        error(e) {
          console.log('从服务器加载可用空间时出现错误')
          console.log(e)
        }
      })
  }

  updateSpaceProgress(total: number, free: number) {
    this.total = total
    this.used = total - free
    this.percent = (this.used / this.total) * 100
    this.state = this.viewService.storageStage(this.percent)
  }

  onSideBarCollapsed(state: boolean) {
    this.isCollapsed = state
  }

  toggleTheme(): void {
    this.themeService.toggleTheme().then();
  }

  toggleSideBar() {
    this.isCollapsed = !this.isCollapsed
  }

  toggleMultiChoice() {
    this.ctx.toggleMultiChoice()
    this.topMultiChoiceBtnChange()
  }

  private topMultiChoiceBtnChange() {
    if (this.ctx.enableMultipleChoice) {
      // multiChoiceBtn.style.backgroundColor = ""
    } else {

    }
  }
}
