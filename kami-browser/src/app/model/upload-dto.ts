export interface UploadTaskResponse {
  success: boolean
  message: string
  taskId: string
  error: UploadFileErrorReporter | null
  files: ActualUploadFile[]
}

export interface UploadFileErrorReporter {
  uploadPath: string
  endpoint: string
  limit: number
  free: number
}

export interface ActualUploadFile {
  uploadFilename: string
  realFilename: string
  progress: boolean[]
  filesize: number
  chunkedSize: number
  chunkCount: number
  successCount: number
  done: boolean
}
