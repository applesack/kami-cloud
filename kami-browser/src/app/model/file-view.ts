export interface FileDesc {
  path: string,
  size: number
  filename: string
  filetype: string
  created: string
  updated: string
  priority: number
  icon: string
  checked: boolean
  disabled: boolean
  selected: boolean
}

export interface DirectoryCapacity {
  path: string
  limit: number
  free: number
}

export interface FileRecords {
  uid: number
  uploader: string
  remark: string
  downloadCount: number
}

export interface FileRuleInfo {
  inherit: string
  rule: number[]
}

export interface FileCapInfo {
  inherit: string
  limit: number
  free: number
}

export interface FileDetails {
  desc: FileDesc
  record: FileRecords
  rule: FileRuleInfo
  cap: FileCapInfo
}
