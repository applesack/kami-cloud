import {FileDesc} from "./file-view";

export interface Broadcast<T> {
  timestamp: number
  topic: string
  title: string
  code: number
  payload: T
}

export interface FileDeleteEvent {
  path: string
  filename: string
}

export interface FileRenameEvent {
  path: string
  oldName: string
  newName: string
}

export interface FileAddEvent {
  path: string
  filename: string
  file: FileDesc
}

export interface CapacityChangeEvent {
  path: String
  limit: number
  free: number
}
