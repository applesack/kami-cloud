import {UploadFileErrorReporter} from "./upload-dto";
import Timeout = NodeJS.Timeout;

export interface UploadTask {
  contentLoading: boolean
  title: string
  tag: string
  tagColor: string
  tagIcon: string
  btnDisable: boolean
  btnLoading: boolean
  taskId: string
  state: number
  uploadPath: string
  success: boolean
  errorMsg: string | null
  percent: number
  progressAutoCheckerId: number
  error: UploadFileErrorReporter | null
  subTasks: SingleUploadTask[]
  nativeFiles: File[],
  totalSize: number,
  availableCount: number
}

export interface SingleUploadTask {
  errMessage: string
  btnIcon: string
  btnLoading: boolean
  btnDisable: boolean
  deleteBtnVisible: boolean
  state: number
  parentRef: UploadTask
  progress: boolean[]
  percent: number
  filename: string
  file: File
  realFilename: string
  chunkedSize: number
  successCount: number
  ctrl: ChunkUploadRequest
}

export interface ChunkUploadRequest {
  chunkSequence: number[]
  reqs: Map<number, RequestProgress>
}

export interface RequestProgress {
  req: XMLHttpRequest,
  percent: number
}
