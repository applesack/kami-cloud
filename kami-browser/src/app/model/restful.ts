export interface Result<T> {
  success: boolean
  message: string
  code: number
  data: T | null
  timestamp: number
}
