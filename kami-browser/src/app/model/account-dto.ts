export interface EncodedToken {
  clientId: string
  publicKey: string
}

export interface AccountDTO {
  username: string
  password: string
  token?: string
  clientId: string
}

export interface AccountFrom {
  username: string
  password: string
  token?: string
}
