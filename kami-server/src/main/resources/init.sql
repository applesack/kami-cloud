DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS rules;
DROP TABLE IF EXISTS limits;
DROP TABLE IF EXISTS logs;
DROP TABLE IF EXISTS tasks;
DROP TABLE IF EXISTS files;

CREATE TABLE users
(
    id       INTEGER     NOT NULL PRIMARY KEY AUTOINCREMENT,
    username VARCHAR(32) NOT NULL UNIQUE,
    password VARCHAR(34) NOT NULL,
    salt     VARCHAR(16) NOT NULL,
    role     INTEGER(4)  NOT NULL,
    created  INTEGER(11) NOT NULL
);

CREATE TABLE rules
(
    id          INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
    created     BIGINT       NOT NULL,
    updated     BIGINT       NOT NULL,
    path        VARCHAR(512) NOT NULL,
    access_rule BLOB         NOT NULL
);

CREATE TABLE limits
(
    id       INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
    created  BIGINT       NOT NULL,
    updated  BIGINT       NOT NULL,
    path     VARCHAR(512) NOT NULL,
    capacity INTEGER(11)  NOT NULL
);

CREATE TABLE logs
(
    id       INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
    type     VARCHAR(32)  NOT NULL,
    operator INTEGER(11)  NOT NULL,
    subject  VARCHAR(128) NOT NULL,
    desc     VARCHAR(512) NOT NULL,
    date     INTEGER(11)  NOT NULL
);

CREATE TABLE tasks
(
    id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    type    INTEGER NOT NULL,
    state   INTEGER NOT NULL,
    user    INTEGER NOT NULL,
    created BIGINT  NOT NULL,
    payload VARCHAR NOT NULL
);

CREATE TABLE files
(
    id       INTEGER      NOT NULL PRIMARY KEY,
    filepath VARCHAR(512) NOT NULL,
    owner    INTEGER      NOT NULL,
    created  BIGINT       NOT NULL
);

CREATE INDEX file_filepath_idx ON files (filepath);