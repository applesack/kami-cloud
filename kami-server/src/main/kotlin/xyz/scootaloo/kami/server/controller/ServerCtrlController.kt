package xyz.scootaloo.kami.server.controller

import io.vertx.ext.web.Router
import xyz.scootaloo.kami.server.standard.getLogger

/**
 * @author flutterdash@qq.com
 * @since 2021/12/31 18:10
 */
object ServerCtrlController : BaseController() {
    override val log by lazy { getLogger() }
    override val mountPoint = "/ctrl"

    override fun doRoute(router: Router) = router.apply {
        get("/shutdown") {

        }

        get("/restart") {

        }
    }
}