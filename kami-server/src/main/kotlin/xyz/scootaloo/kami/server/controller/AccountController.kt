package xyz.scootaloo.kami.server.controller

import io.vertx.ext.web.Router
import xyz.scootaloo.kami.server.controller.dto.AccountForm
import xyz.scootaloo.kami.server.standard.getLogger
import xyz.scootaloo.kami.server.service.AccountService
import xyz.scootaloo.kami.server.service.HttpMessageHelper
import xyz.scootaloo.kami.server.service.TokenService

/**
 * @author flutterdash@qq.com
 * @since 2022/1/31 10:20
 */
object AccountController : BaseController() {

    private val tokenService = TokenService()
    private val accountService = AccountService()

    override val log by lazy { getLogger() }
    override val mountPoint = "/account"

    override fun doRoute(router: Router) = router.apply {

        get("/") { ctx ->
            val ipAddress = HttpMessageHelper.getIpAddress(ctx.request())
            val token = tokenService.generateToken(ipAddress)
            ctx.rest(token)
        }

        post("/register") { ctx ->
            val form = ctx.readBodyAsDTO(AccountForm::class)
            val jwt = accountService.register(ctx.request(), form)
            ctx.rest(jwt)
        }

        post("/login") { ctx ->
            val form = ctx.readBodyAsDTO(AccountForm::class)
            val jwt = accountService.login(ctx.request(), form)
            ctx.rest(jwt)
        }

        post("/logout") {
        }

    }
}