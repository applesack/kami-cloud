package xyz.scootaloo.kami.server.service

import xyz.scootaloo.kami.server.service.impl.InternalSystemServiceImpl

/**
 * @author flutterdash@qq.com
 * @since 2022/3/11 15:46
 */
interface SystemService {

    companion object {
        operator fun invoke(): SystemService {
            return InternalSystemServiceImpl
        }
    }

    /**
     * 调用 System.gc()
     * 这个操作是异步的
     */
    fun gc()

    /**
     * 获取当前方法的调用者, 包括(类名, 方法名, 行号)
     */
    fun getCaller(): String

}