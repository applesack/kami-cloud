package xyz.scootaloo.kami.server.model.dao

import org.ktorm.entity.add
import xyz.scootaloo.kami.server.model.SysLog
import xyz.scootaloo.kami.server.model.logs
import xyz.scootaloo.kami.server.verticle.database

/**
 * @author flutterdash@qq.com
 * @since 2022/2/13 10:46
 */
object LogDAO {

    fun store(log: SysLog) {
        database.logs.add(log)
    }

}