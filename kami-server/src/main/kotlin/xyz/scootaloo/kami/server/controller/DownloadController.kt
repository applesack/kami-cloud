package xyz.scootaloo.kami.server.controller

import io.vertx.ext.web.Router
import io.vertx.kotlin.coroutines.await
import xyz.scootaloo.kami.server.service.DownloadService
import xyz.scootaloo.kami.server.service.HttpMessageHelper
import xyz.scootaloo.kami.server.standard.getLogger

/**
 * @author flutterdash@qq.com
 * @since 2022/1/9 14:52
 */
object DownloadController : BaseController() {

    private val downloadService = DownloadService()

    override val log by lazy { getLogger() }
    override val mountPoint = "/download"

    override fun doRoute(router: Router) = router.apply {
        get("/*") { ctx ->
            val filename = ctx.pathParam("*")
            val token = ctx.request().getParam("t")
            val ipAddress = HttpMessageHelper.getIpAddress(ctx.request())
            downloadService.download(ctx, filename, ipAddress, token).await() // 强制使异常抛出
        }
    }
}