登录界面
    - 点击注册按钮跳转到注册界面
    - 点击登录按钮执行登录
注册界面
    - 点击登录按钮跳转到登录页面
    - 点击注册按钮执行注册

主页面
    - 显示默认目录的文件列表, 默认多选关闭, 顶部菜单关闭, 没有选中文件
    - 单击文件, 选中文件
        - 如果开启了多选模式, 则单击文件会将当前文件添加到多选列表
        - 如果这是一个普通文件(不是目录), 弹出顶部菜单
    - 双击文件, 尝试打开文件; 关闭多选, 清空多选列表, 关闭顶部菜单
        - 如果这个文件是目录, 则跳转到这个目录
        - 如果这个文件不是目录, 检查是否是可打开的类型, 如果是, 则尝试打开, 否则告诉用户该文件不可打开
    - 多选模式
        - 开启多选模式: 高亮多选按钮, 顶部菜单中, 隐藏编辑,打开,分享按钮