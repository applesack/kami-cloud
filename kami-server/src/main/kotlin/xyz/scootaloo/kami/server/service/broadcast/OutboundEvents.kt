package xyz.scootaloo.kami.server.service.broadcast

import xyz.scootaloo.kami.server.standard.InternalApi
import xyz.scootaloo.kami.server.standard.currentTimeMillis

/**
 * eventbus出站事件
 *
 * @author flutterdash@qq.com
 * @since 2022/3/12 14:03
 */

interface EventPayload

interface OutboundEvent<T : EventPayload> {
    val timestamp: Long get() = currentTimeMillis()
    val topic: String
    val title: String
    val code: Int
    val payload: T

    companion object {
        @InternalApi
        internal fun <T : EventPayload> create(type: BroadcastEventType, payload: T): OutboundEvent<T> {
            return SimpleOutboundEvent(type, payload)
        }
    }
}

enum class BroadcastEventType(val code: Int, val topic: String, val title: String) {
    FILE_ADD(1001, "file", "add"),
    FILE_DEL(1002, "file", "delete"),
    FILE_RENAME(1003, "file", "rename"),
    FILE_CAP(1005, "file", "capacity")
}

class SimpleOutboundEvent<T : EventPayload>(
    type: BroadcastEventType, override val payload: T
) : OutboundEvent<T> {
    override val code = type.code
    override val topic = type.topic
    override val title = type.title
}

abstract class FileModifyEventPayload(open val path: String) : EventPayload