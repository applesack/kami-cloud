package xyz.scootaloo.kami.server.service

import xyz.scootaloo.kami.server.service.impl.InternalFileLogServiceImpl

/**
 * 记录文件变动事件
 *
 * 这个接口中所有的方法被调用都代表某操作执行成功
 *
 * @author flutterdash@qq.com
 * @since 2022/3/10 23:06
 */
interface FileLogService {

    companion object {
        operator fun invoke(): FileLogService {
            return InternalFileLogServiceImpl
        }
    }

    fun onFileUpload(operator: Int, basePath: String, filename: String)

    fun onFileRename(operator: Int, basePath: String, oldName: String, newName: String)

    fun onFileCreate(operator: Int, basePath: String, filename: String)

    fun onFileDelete(operator: Int, basePath: String, filename: String)

    fun onFileMove(operator: Int, oldFilepath: String, filename: String, newFilepath: String)

    fun onFileModify(operator: Int, basePath: String, filename: String)

    fun onDirectoryCreate(operator: Int, basePath: String, dirname: String)

    fun onDirectoryDelete(operator: Int, filepath: String)

    fun onCapacityChange(dirPath: String, limit: Long, free: Long)

    interface FileLog {
        val subject: String
        val act: String
    }

    data class FileCreateLog(override val subject: String, override val act: String = "create") : FileLog
    data class FileRenameLog(override val subject: String, val newPath: String, override val act: String = "rename") : FileLog
    data class FileDeleteLog(override val subject: String, override val act: String = "delete") : FileLog
    data class FileMoveLog(override val subject: String, val destPath: String, override val act: String = "move") : FileLog
}