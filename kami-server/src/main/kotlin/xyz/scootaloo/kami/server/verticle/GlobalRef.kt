package xyz.scootaloo.kami.server.verticle

import io.vertx.core.Vertx
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import org.ktorm.database.Database

/**
 * @author flutterdash@qq.com
 * @since 2022/2/3 19:04
 */
typealias CoroutineBlock = suspend CoroutineScope.() -> Unit

lateinit var vertx: Vertx

lateinit var database: Database

fun runCoroutineOnWorkerContext(block: CoroutineBlock): Job {
    return WorkerDeployer.runCoroutineOnContext(block)
}

fun runCoroutineOnEventLoopContext(block: CoroutineBlock): Job {
    return EventLoopDeployer.runCoroutineOnContext(block)
}

fun setGlobalDatabaseRef(db: Database) {
    database = db
}

fun setGlobalVertxRef(v: Vertx) {
    vertx = v
}