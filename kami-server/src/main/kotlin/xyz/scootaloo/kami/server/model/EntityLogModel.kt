@file:Suppress("unused")

package xyz.scootaloo.kami.server.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.varchar
import xyz.scootaloo.kami.server.standard.currentTimeMillis

/**
 * @author flutterdash@qq.com
 * @since 2022/2/13 10:34
 */

interface SysLog : Entity<SysLog> {
    var id: Int
    var operator: Int
    var subject: String
    var type: String
    var desc: String
    var date: Long

    companion object : Entity.Factory<SysLog>() {
        fun create(operator: Int, subject: String, type: String, desc: String): SysLog {
            return SysLog {
                this.operator = operator
                this.subject = subject
                this.type = type
                this.desc = desc
                this.date = currentTimeMillis()
            }
        }
    }
}

object SysLogs : Table<SysLog>("logs") {
    val id = int("id").primaryKey().bindTo { it.id }
    val operator = int("operator").bindTo { it.operator }
    val subject = varchar("subject").bindTo { it.subject }
    val type = varchar("type").bindTo { it.type }
    val desc = varchar("desc").bindTo { it.desc }
    val date = long("date").bindTo { it.date }
}

val Database.logs get() = sequenceOf(SysLogs)