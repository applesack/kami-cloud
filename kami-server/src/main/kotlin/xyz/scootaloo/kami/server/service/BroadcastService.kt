package xyz.scootaloo.kami.server.service

import xyz.scootaloo.kami.server.service.broadcast.OutboundEvent
import xyz.scootaloo.kami.server.service.impl.InternalBroadcastServiceImpl

/**
 * 广播服务
 *
 * 通过这个接口，将消息传播到指定的客户端去;
 * 只负责将消息广播到客户端, 不负责产生和处理消息
 *
 * @author flutterdash@qq.com
 * @since 2022/3/11 18:20
 */
interface BroadcastService {

    companion object {
        operator fun invoke(): BroadcastService {
            return InternalBroadcastServiceImpl
        }
    }

    fun publish(event: OutboundEvent<*>)

}