@file:Suppress("unused")

package xyz.scootaloo.kami.server.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.varchar

/**
 * @author flutterdash@qq.com
 * @since 2022/2/1 0:23
 */

interface SysUser : Entity<SysUser> {
    var id: Int
    var username: String
    var password: String
    var salt: String
    var role: Int
    var created: Long

    companion object : Entity.Factory<SysUser>()
}

object SysUsers : Table<SysUser>("users") {
    val id = int("id").primaryKey().bindTo { it.id }
    val username = varchar("username").bindTo { it.username }
    val password = varchar("password").bindTo { it.password }
    val salt = varchar("salt").bindTo { it.salt }
    val role = int("role").bindTo { it.role }
    val created = long("created").bindTo { it.created }
}

val Database.users get() = this.sequenceOf(SysUsers)