package xyz.scootaloo.kami.server.standard

import kotlin.reflect.KClass

/**
 * 内部注解
 *
 * @author flutterdash@qq.com
 * @since 2022/1/12 16:58
 */

/**
 * vertx-worker的标记
 *
 * 由于系统内worker的设计是声明和实例化是分离的, 为了快捷的查看一个worker对象的一些重要属性, 使用注解标识worker
 *
 * @param poolSize 创建worker时指定线程的数量
 * @param name     worker的唯一标识
 */
@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.SOURCE)
annotation class WorkerPool(val poolSize: Int, val name: String = "")

/**
 * 禁止方法重复调用的标记
 *
 * 有些执行初始化的方法, 尤其在本系统内, 需要在线程安全的环境下执行初始化的,
 * 满足只能被调用一次的, 标记此注解
 *
 * @param caller 唯一的调用者所在的位置
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class DisallowRepeatedCalls(val caller: KClass<*>)

/**
 * 限制调用者
 *
 * 标记一个方法只能在指定的位置调用
 *
 * @param callers 被允许调用的调用者列表
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class LimitCallers(val callers: Array<KClass<*>>)

/**
 * 标记一个类, 或者方法暂时无法提供功能
 *
 * @param desc 其他描述
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class Unavailable(val desc: String = "")

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class InternalApi

/**
 * 仅允许在线程安全的环境下调用
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD)
@Retention(AnnotationRetention.SOURCE)
annotation class ThreadSafetyOnly

/**
 * 标记一个方法或者属性, 只允许在线程安全的环境下访问
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.SOURCE)
annotation class SyncMark

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.EXPRESSION)
@Retention(AnnotationRetention.SOURCE)
annotation class AsyncMark

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.EXPRESSION)
@Retention(AnnotationRetention.SOURCE)
annotation class Type(val type: KClass<*>)

/**
 * 标记一个方法是耗时的操作, 不会立刻返回, 可能会阻塞当前线程; 调用时应该注意
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class NaughtyStep