package xyz.scootaloo.kami.server.controller

import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler
import xyz.scootaloo.kami.server.standard.getLogger
import xyz.scootaloo.kami.server.service.FileResolver

/**
 * @author flutterdash@qq.com
 * @since 2022/1/21 23:44
 */
object StaticAssetsController : BaseController() {

    private val fileResolver = FileResolver()

    override val log by lazy { getLogger() }
    override val mountPoint = "/static"

    override fun doRoute(router: Router) = router.apply {
        get("/*").handler(
            StaticHandler
                .create()
                .setAllowRootFileSystemAccess(true)
                .setWebRoot(fileResolver.assets())
        )
    }
}