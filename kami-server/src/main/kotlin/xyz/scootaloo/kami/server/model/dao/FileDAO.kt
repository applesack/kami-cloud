package xyz.scootaloo.kami.server.model.dao

import org.ktorm.dsl.eq
import org.ktorm.entity.add
import org.ktorm.entity.find
import xyz.scootaloo.kami.server.model.SysFile
import xyz.scootaloo.kami.server.model.files
import xyz.scootaloo.kami.server.verticle.database

/**
 * @author flutterdash@qq.com
 * @since 2022/3/16 13:17
 */
object FileDAO {

    fun store(file: SysFile) {
        database.files.add(file)
    }

    fun find(filepath: String): SysFile? {
        return database.files.find { it.filepath eq filepath }
    }

    fun delete(filepath: String) {
        database.files.find { it.filepath eq filepath }?.delete()
    }

    fun move(oldFilepath: String, newFilepath: String) {
        val record = database.files.find { it.filepath eq oldFilepath } ?: return
        record.filepath = newFilepath
        record.flushChanges()
    }

}