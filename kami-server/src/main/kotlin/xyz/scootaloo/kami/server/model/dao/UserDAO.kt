package xyz.scootaloo.kami.server.model.dao

import org.ktorm.dsl.eq
import org.ktorm.entity.add
import org.ktorm.entity.find
import xyz.scootaloo.kami.server.model.SysUser
import xyz.scootaloo.kami.server.model.users
import xyz.scootaloo.kami.server.verticle.database

/**
 * @author flutterdash@qq.com
 * @since 2022/2/7 23:13
 */
object UserDAO {

    fun findByName(username: String): SysUser? {
        return database.users.find { it.username eq username }
    }

    fun findByUid(uid: Int): SysUser? {
        return database.users.find { it.id eq uid }
    }

    fun store(user: SysUser) {
        user.created = System.currentTimeMillis()
        database.users.add(user)
    }

}