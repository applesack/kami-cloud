package xyz.scootaloo.kami.server.controller

import io.vertx.ext.web.Router
import xyz.scootaloo.kami.server.service.HttpMessageHelper
import xyz.scootaloo.kami.server.standard.getLogger

/**
 * @author flutterdash@qq.com
 * @since 2021/12/31 18:39
 */
object HeartbeatController : BaseController() {
    override val log by lazy { getLogger() }
    override val mountPoint = "/ping"

    override fun doRoute(router: Router): Router {
        return router.apply {
            get("/*") { ctx ->
                val remote = HttpMessageHelper.getIpAddress(ctx.request())
                log.info("来自${remote}的请求, uri:${ctx.request().uri()}")
                ctx.response().end("hello: $remote")
            }
        }
    }
}