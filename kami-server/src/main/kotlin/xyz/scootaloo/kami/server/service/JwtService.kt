package xyz.scootaloo.kami.server.service

import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.User
import xyz.scootaloo.kami.server.model.UserPrincipal
import xyz.scootaloo.kami.server.service.impl.InternalJwtServiceImpl

/**
 * @author flutterdash@qq.com
 * @since 2022/2/6 15:58
 */
interface JwtService {

    companion object {
        operator fun invoke(): JwtService {
            return InternalJwtServiceImpl
        }
    }

    /**
     * 签发 jwt
     */
    fun issueJWT(tokenObject: UserPrincipal): String

    /**
     * 验证 jwt
     */
    fun authenticate(jwt: String): Future<User>

    /**
     * 返回访客用户(未登录用户的身份标识)
     */
    fun guestUserPrincipal(): JsonObject
}