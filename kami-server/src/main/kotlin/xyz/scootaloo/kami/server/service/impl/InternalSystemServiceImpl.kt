package xyz.scootaloo.kami.server.service.impl

import xyz.scootaloo.kami.server.standard.getLogger
import xyz.scootaloo.kami.server.service.ApplicationStageListener
import xyz.scootaloo.kami.server.service.Async
import xyz.scootaloo.kami.server.service.SystemService

/**
 * @author flutterdash@qq.com
 * @since 2022/3/11 15:47
 */
object InternalSystemServiceImpl : SystemService, ApplicationStageListener {

    private val log by lazy { getLogger() }

    override fun afterApplicationStarted() {
        gc()
    }

    override fun gc() {
        val caller = getCaller()
        log.warn("调用GC <== $caller")
        Async.run { System.gc() }
    }

    override fun getCaller(): String {
        /**
         * 0. 调用getStackTrace()
         * 1. 调用当前方法 getCaller()
         * 2. 此方法的调用者
         * 3. 实际调用者
         */
        val stack = Thread.currentThread().stackTrace
        val method = stack[3]
        return "[${method.className}#${method.methodName}(:${method.lineNumber})]"
    }

}