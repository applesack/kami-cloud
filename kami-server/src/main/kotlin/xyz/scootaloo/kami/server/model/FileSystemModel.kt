package xyz.scootaloo.kami.server.model

/**
 * @author flutterdash@qq.com
 * @since 2022/1/3 14:44
 */
typealias EFile = EntityFile

data class EntityFile(
    val path: String = "",
    val size: Long = 0,
    val filename: String = "",
    val filetype: String = "",
    val created: String = "",
    val updated: String = "",
)

data class DirectoryCapacity(
    val path: String,
    val limit: Long,
    val free: Long
)

data class SingleUploadFileInfo(
    val filename: String,
    val filesize: Long,
    val lastModified: Long
)

data class UploadExceedLimitReporter(
    val uploadPath: String,
    val endpoint: String,
    val limit: Long,
    val free: Long
)

data class CreateIllegalLimitReporter(
    val createPath: String,
    val endpoint: String,
    val limit: Long
)

data class UploaderRecord(
    val uid: Int,
    val uploader: String,
    val remark: String,
    val downloadCount: Int
)

data class FileRuleInfo(
    val inherit: String,
    val rule: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FileRuleInfo

        if (inherit != other.inherit) return false

        return true
    }

    override fun hashCode(): Int {
        return inherit.hashCode()
    }
}

data class FileCapInfo(
    val inherit: String,
    val limit: Long,
    val free: Long
)

data class FileDetails(
    val desc: EntityFile,
    val record: UploaderRecord,
    val rule: FileRuleInfo,
    val cap: FileCapInfo
)