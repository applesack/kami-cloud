package xyz.scootaloo.kami.server.service.impl

import xyz.scootaloo.kami.server.standard.toJsonObject
import xyz.scootaloo.kami.server.service.BroadcastService
import xyz.scootaloo.kami.server.service.broadcast.OutboundEvent
import xyz.scootaloo.kami.server.verticle.vertx

/**
 * @author flutterdash@qq.com
 * @since 2022/3/13 13:16
 */
object InternalBroadcastServiceImpl : BroadcastService {

    private val bus get() = vertx.eventBus()

    override fun publish(event: OutboundEvent<*>) {
        bus.publish(event.eventbusAddress(), event.json())
    }

    private fun OutboundEvent<*>.eventbusAddress(): String {
        return "${topic}.${title}"
    }

    private fun OutboundEvent<*>.json(): String {
        return toJsonObject().toString()
    }
}