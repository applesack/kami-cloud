package xyz.scootaloo.kami.server.service

/**
 * @author flutterdash@qq.com
 * @since 2022/3/17 11:47
 */

data class AppConfig(
    var serverConfig: ServerConfig = ServerConfig(),
    var databaseConfig: DatabaseConfig = DatabaseConfig(),
    var fileConfig: FileConfig = FileConfig()
)

data class ServerConfig(
    var port: Int = 9090
)

data class DatabaseConfig(
    var path: String = "./conf/kami.db"
)

data class FileConfig(
    var homePath: String = "./home",
    var assetPath: String = "./assets",
    var cachePath: String = "./file-uploads"
)