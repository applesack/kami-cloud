(2022/2/9)
-----------------------------------------------------
1. 规则结构

一条文件系统的权限规则由2个部分组成
    1. 路径规则, 一个文件的路径, 末尾是*号代表此规则应用于整个目录(包括此目录的子目录), 不以*号结尾的代表只限制一个文件
    2. 权限规则, 用byte数组标识, 一共有4种规则限制(数组长度4), 按照顺序分别是[修改, 创建, 下载, 访问]
例如规则是 { pathReg: '/home/*', rule: 3321 }
访问者访问目录 /home/a.txt, 访问者的等级是(修改需要3, 创建需要3, 下载需要2, 访问需要1)
    1. /home/a.txt 可以和路径 /home/* 匹配, 所以访问者的操作服从规则
    2. 由于规则是3321, 代表用户需要具有管理员(3)权限才能下载, 登录(1)后才能访问, 其余类型只有超级管理员能访问;
       由于此用户的等级是2, 所以他可以访问和下载, 但是不能创建和修改 (访客用户等级0, 登录用户1, 管理员2, 超级管理员3)

-----------------------------------------------------
2. 前缀树的数据结构 ┌┬┐
    假设当前数据库中存在4条规则
    { pathReg: '/home/*', rule: 0011}
    { pathReg: '/home/file/doc/*', rule: 0021}
    { pathReg: '/home/file/a.txt', rule: 0011}
    { pathReg: '/dev/file/*', rule: 0020, level: 2 }
    生成树的结构是:
                        / 根目录
               ┌--------+------------┐
             /dev                 @/home
          ┌----+              ┌-----+
        @/file              /file
                          ┌---+----┐
                        @/doc    @a.txt

 树的最小单位是节点
 在这个树中, 一共有7个节点, 但是规则只有4条(用 @ 符号标记)
 其中节点/dev和/home下的子节点/file是空节点, 只是为了连接上下级而存在的, 不具备拦截功能, 所以它的level可以设置成0

 树节点的数据结构

 PathItemNode {
    itemKey: String
    rule: ByteArray
    parent: PathItemNode?
    subItems: HashMap<String, PathItemNode>
 }


-----------------------------------------------------
3. 前缀树的插入, 删除, 匹配

前缀树始终有一个根节点, 且这个节点不能被删除

a. 插入 (修改)
    假设要插入一条规则 { pathReg: '/home/file/doc/book/*', rule: 3321 } 插入一个树中不存在的节点
        1. 首先 将pathReg按照/分割, 得到['home', 'file', 'doc', 'book']
        2. 首先初始化pos = 0, 代表将从数组第0个位置, 插入到根目录
        3. 由于根目录下已存在home这个键, 于是pos+1, 从数组第1个位置插入到home, 此时pos = 1
        4. 将['file', 'doc', 'book']插入到home, 由于home下存在file这个键, pos+1
        5. 将['doc', 'book']插入到file, pos+1
        6. 将['book']插入到doc, 此时pos=3, 指向数组的最后一个元素, 但目录doc下没有值为book的键,
           把book作为新节点插入到doc中, doc.subItems.put('book', {'book', 3321, {})
           插入完成
    插入一个规则, 这个规则在树中已存在(路径相同但是规则不同) { pathReg: '/home', rule: 1122 }
        1. 将['home']的规则插入到根目录, 由于根目录已存在home这个节点, 所以只需要对这个节点的规则进行更新

b. 删除
    删除路径 '/home/file/doc' 上的规则
        1. 重复插入逻辑的1~5, 找到这个节点, 假如这个节点不存在, 则无需删除
        2. 检查这个节点有没有子节点
            a. 假如没有, 则找到父节点, 删除当前节点
            b. 假如有, 将这个节点的规则清空(设置成 0000),
    考虑下面这种节点删除的情况
                        / 根目录
                        +------------┐
                                  @/home
                               ┌-----+
                             /file
                           ┌---+
                         @/doc
    删除在 '/home/file/doc' 上的规则
    由于doc的父节点file是一个空节点, 在doc节点被删除后, file节点不具备实际意义, file节点也应该被删除,
    所以删除动作应该是递归的, 向上查找, 伪代码
        currentNode = theNode
        while (!current.parent.isValid) {
            currentNode = currentNode.parent
        }
        currentNode.parent.remove(currentNode.key)
    上面的案例在节点删除后应该是这样的结构
                       / 根目录
                       +------------┐
                                 @/home

c. 匹配
    匹配逻辑参考(4.api设计和功能实现)

-----------------------------------------------------
4. api设计和功能实现

需要实现的功能
    a. 查看是否具备某个文件的访问权限
    b. 查看某个文件的详细信息
    c. 返回目录内容(目录下所有文件的信息, 不包括子目录)

方法原型
access(path: String, userLevel: Int, type: AccessType): PathItemNode
    方法参数
        path 用户要访问的路径
        userLevel 用户的等级
        type 访问操作的类型(访问, 下载, 创建, 修改)
    返回值
        返回这个路径设定的规则, 假如前缀树中没有这个路径对应的规则, 那么会返回最后匹配上的节点的规则,
        即子节点使用父节点的规则
    方法实现
        将要访问的路径按照/分隔符拆分成数组, 进入前缀树进行匹配, 每到达一个节点, 就把当前的操作类型和需要的权限进行比较,
        假如从跟节点到实际叶子节点中任意一个节点的权限不足, 则直接抛出异常, 交给异常处理器来处理;
        到达叶子节点后(叶子节点允许访问), 则返回叶子节点

功能实现
    a. 查看是否具备某个文件的访问权限
        调用access方法, 假如具备这个文件/目录的查看权限, 则将权限信息封装成pojo返回,
        假如access方法没有返回而是直接抛出了异常, 则代表用户没有这个文件/目录的查看权限
    b. 查看某个文件的详细信息
        参考a的实现, 需要查询数据库
    c. 返回目录的内容
        调用access后, 返回这个目录的节点, 然后搜索文件系统查找实际目录的文件列表,
        检查列表中每一个文件是否存在于返回的规则节点中
            a. 存在, 检查是否允许访问, 假如允许, 则放入结果, 否则忽略这个文件
            b. 不存在, 则这个文件没有被限制, 则直接添加到结果

    对于功能c的实现, 需要考虑以下这种情况, 父子节点的相似性:
           前缀树 / 根目录                     实际文件系统 / 根目录
                +------------┐                         +------------┐
                            @/a                                    /a
                       ┌-----+-----┐                          ┌-----+-----┐
                      /a          @/b                        /a           /b
                 ┌-----+-----┐                          ┌-----+-----┐
                             /b                         c           /b
                       ┌-----+-----┐                          ┌-----+-----┐
                      ...         @/x                         z          /x

            目录 '/a'有规则 0020
            目录 '/a/b' 有规则 0021

            用户具有的访问权限是0
            此时用户希望查看目录 '/a/a'的下的文件内容,
                由于'/a/a'的父节点'/a'上有规则限制, 但是'/a/a'上没有规则,
                所以'/a/a'的规则是继承于父节点的, 也就是说调用access('/a/a')返回值等同于access('/a')
                预期返回的文件列表是 [c, /b], 但是实际得到的结果是 [c]

                问题出在目录'/a/a'使用了父目录'/a'的权限规则, 但是这两个目录都有一个同名的子文件'/b'

                解决方案: 仅访问权限继承于父节点, 子目录的结构仍然不变, 因此access在这种情况应该返回一个新的节点


-----------------------------------------------------
5. 算法的性能分析

时间复杂度取决于规则树的深度和访问目标目录的深度
设用户访问的目录深度为m, 规则树深度为n, 时间复杂度是O(min(m, n))

-----------------------------------------------------
6. 线程安全的动态更新规则

singleThread缓存线程          thread-0              thread-1              thread-n

任意线程需要访问前缀树的时候, 同步的更新使用树标记

            usingCount = 0  这个标记代表当前正在使用前缀树的线程数, 最开始还没有任何线程使用的时候, 使用数为0

            // 获取前缀树, 假如同时有其他线程正在更新标记, 则当前线程会被阻塞
            fun acquire() {
                // 在同步块中进行更新使用数
                synchronized {
                    usingCount += 1
                }
            }

            // 前缀树使用完毕, 归还前缀树
            fun release() {
                // 同步更新标记, 正常情况下, 使用-释放 是一对操作, 相互抵消, 最终前缀数空闲时会变为0
                synchronized {
                    usingCount -= 1
                    if (usingCount == 0) {
                        for (lambda in singleThread.tasks) {
                            lambda.invoke() // 在同步锁内完成树的更新
                        }
                    }
                }
            }

            // 直接异步提交更新动作, 非阻塞
            fun update(lambda) {
                singleThread.submit(lambda)
            }

