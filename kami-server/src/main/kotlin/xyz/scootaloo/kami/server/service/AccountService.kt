package xyz.scootaloo.kami.server.service

import io.vertx.core.http.HttpServerRequest
import xyz.scootaloo.kami.server.controller.dto.AccountForm
import xyz.scootaloo.kami.server.service.impl.InternalAccountServiceImpl

/**
 * @author flutterdash@qq.com
 * @since 2022/3/20 11:51
 */
interface AccountService {

    companion object {
        operator fun invoke(): AccountService {
            return InternalAccountServiceImpl
        }
    }

    suspend fun register(request: HttpServerRequest, form: AccountForm): String
    suspend fun login(request: HttpServerRequest, form: AccountForm): String
    suspend fun logout()

}