package xyz.scootaloo.kami.server.controller.dto

import xyz.scootaloo.kami.server.model.SingleUploadFileInfo

/**
 * @author flutterdash@qq.com
 * @since 2022/2/5 22:52
 */

class UploadPermissionCheck : BaseDTO {
    lateinit var path: String
}

class UploadFileInfo : BaseDTO {
    lateinit var path: String
    lateinit var files: List<UploadFile>
}

class UploadFile : BaseDTO {
    lateinit var filename: String
    var size: Long = 0
    var lastModified: Long = 0

    fun toDesc() = SingleUploadFileInfo(
        filename, size, lastModified
    )
}