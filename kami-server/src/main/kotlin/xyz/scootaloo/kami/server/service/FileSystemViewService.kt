package xyz.scootaloo.kami.server.service

import xyz.scootaloo.kami.server.model.*
import xyz.scootaloo.kami.server.service.impl.InternalFileSystemViewServiceImpl

/**
 * 文件系统视图工具
 *
 * 面向Controller等应用层，这个接口存在的意义是屏蔽对文件系统的复杂操作
 * 只暴露出简单明了的接口供上传模块调用
 *
 * @author flutterdash@qq.com
 * @since 2022/1/11 15:51
 */
interface FileSystemViewService {

    companion object {
        operator fun invoke(): FileSystemViewService {
            return InternalFileSystemViewServiceImpl
        }
    }

    suspend fun listDirectoryContent(token: AccessToken): List<EFile>

    suspend fun showFileDesc(token: AccessToken): FileDetails

    suspend fun createFileDownloadLink(token: AccessToken, ipAddress: String): String

    suspend fun createFile(token: AccessToken, filename: String)

    suspend fun createDirectory(token: AccessToken, dirname: String)

    suspend fun renameFile(token: AccessToken, filename: String, newFilename: String)

    suspend fun moveFile(token: AccessToken, filename: String, destDir: String)

    suspend fun deleteFile(token: AccessToken, filename: String)

    suspend fun unzipFile(token: AccessToken, filename: String)

    suspend fun zipFile(token: AccessToken, filename: String)

    suspend fun zipFiles(token: AccessToken, files: List<String>)

    suspend fun zipDirectory(token: AccessToken)

    suspend fun checkUploadPerm(token: AccessToken)

    fun asyncDeleteFileAndUpdateCap(basePath: String, filename: String)

    /**
     * 创建上传任务
     *
     * 0. 检查当前用户是否具有当前目录的上传权限
     * 1. 根据文件列表, 在当前目录下创建临时文件(如果已经存在同名文件, 则使用别名创建新文件)
     * 2. 创建上传任务, 将任务信息写入数据库, 并在放入缓存
     * 3. 创建定时任务, 在上传事件无响应后的两天后删除上传任务
     */
    suspend fun createUploadTask(
        uploader: Int, token: AccessToken, files: List<SingleUploadFileInfo>
    ): UploadFileResponse

    suspend fun displayDirectoryCap(token: AccessToken): DirectoryCapacity
}