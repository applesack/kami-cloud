@file:Suppress("unused")

package xyz.scootaloo.kami.server.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.*
import xyz.scootaloo.kami.server.standard.currentTimeMillis

/**
 * @author flutterdash@qq.com
 * @since 2022/2/8 10:58
 */

interface SysRule : Entity<SysRule> {
    var id: Int
    var created: Long
    var updated: Long
    var path: String
    var accessRule: ByteArray

    companion object : Entity.Factory<SysRule>() {
        fun create(path: String, rule: ByteArray): SysRule {
            return SysRule {
                this.path = path
                this.accessRule = rule
                this.created = currentTimeMillis()
                this.updated = currentTimeMillis()
            }
        }
    }
}

object SysRules: Table<SysRule>("rules") {
    val id = int("id").bindTo { it.id }.primaryKey()
    val created = long("created").bindTo { it.created }
    val updated = long("updated").bindTo { it.updated }
    val path = varchar("path").bindTo { it.path }
    val accessRule = bytes("access_rule").bindTo { it.accessRule }
}

val Database.rules get() = this.sequenceOf(SysRules)