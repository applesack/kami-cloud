package xyz.scootaloo.kami.server.controller.handler

import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.sockjs.SockJSHandler
import io.vertx.kotlin.ext.bridge.permittedOptionsOf
import io.vertx.kotlin.ext.web.handler.sockjs.sockJSBridgeOptionsOf
import io.vertx.kotlin.ext.web.handler.sockjs.sockJSHandlerOptionsOf
import xyz.scootaloo.kami.server.verticle.vertx

/**
 * @author flutterdash@qq.com
 * @since 2022/3/12 19:05
 */

fun attachSockJsHandler(mountPoint: String, router: Router) {
    router.mountSubRouter(mountPoint, sockJSHandler().bridge(bridgeOptions()))
}

private fun sockJSHandler(): SockJSHandler {
    return SockJSHandler.create(vertx, sockJSOptions())
}

private fun sockJSOptions() = sockJSHandlerOptionsOf(
    heartbeatInterval = 2000, maxBytesStreaming = 1024 * 1024
//    origin = "http://192.168.1.107:4200/eventbus" // 开发环境配置
)

private fun bridgeOptions() = sockJSBridgeOptionsOf()
//    .addInboundPermitted(inboundPermitted())
    .addOutboundPermitted(outboundPermittedFile())
    .addOutboundPermitted(outboundPermittedUser())

// 禁止客户端向服务端发送消息
// private fun inboundPermitted() = permittedOptionsOf(addressRegex = "file\\..+")

private fun outboundPermittedFile() = permittedOptionsOf(addressRegex = "file\\..+")
private fun outboundPermittedUser() = permittedOptionsOf(addressRegex = "user\\..+")