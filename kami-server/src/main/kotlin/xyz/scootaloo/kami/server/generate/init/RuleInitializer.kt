package xyz.scootaloo.kami.server.generate.init

import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.runBlocking
import xyz.scootaloo.kami.server.KamiCloud
import xyz.scootaloo.kami.server.model.AccessLevel
import xyz.scootaloo.kami.server.service.RuleSystemService

/**
 * @author flutterdash@qq.com
 * @since 2022/2/21 21:20
 */

fun main() {
    initRuleConfig()
}

private fun initRuleConfig(): Unit = runBlocking {
    val server = KamiCloud()
    server.start().await()

    val service = RuleSystemService()

    rule("").apply {
        access(AccessLevel.GUEST)
        download(AccessLevel.GUEST)
        modify(AccessLevel.USER)
        define(AccessLevel.ADMIN)
        limit = (1024 * 1024 * 1024) * 30L
    }.store(service).await()

    rule("nsfw").apply {
        access(AccessLevel.USER)
        modify(AccessLevel.ADMIN)
    }.store(service).await()

    rule("电影").apply {
        modify(AccessLevel.USER)
        limit = (1024 * 1024 * 1024) * 5L
    }.store(service).await()

    server.shutdown().await()
}

private fun rule(path: String): RuleBuilder {
    return RuleBuilder(path)
}