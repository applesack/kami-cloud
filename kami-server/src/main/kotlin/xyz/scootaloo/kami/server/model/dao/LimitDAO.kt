package xyz.scootaloo.kami.server.model.dao

import org.ktorm.dsl.eq
import org.ktorm.entity.add
import org.ktorm.entity.find
import org.ktorm.entity.toList
import xyz.scootaloo.kami.server.model.SysLimit
import xyz.scootaloo.kami.server.model.limits
import xyz.scootaloo.kami.server.standard.currentTimeMillis
import xyz.scootaloo.kami.server.verticle.database

/**
 * @author flutterdash@qq.com
 * @since 2022/2/22 19:49
 */
object LimitDAO {

    fun list(): List<SysLimit> {
        return database.limits.toList()
    }

    fun store(path: String, limit: Long) {
        var record = database.limits.find { it.path eq path }
        if (record != null) {
            record.updated = currentTimeMillis()
            record.capacity = limit
            record.flushChanges()
        } else {
            record = SysLimit.create(path, limit)
            database.limits.add(record)
        }
    }

    fun delete(path: String) {
        database.limits.find { it.path eq path }?.delete()
    }

}