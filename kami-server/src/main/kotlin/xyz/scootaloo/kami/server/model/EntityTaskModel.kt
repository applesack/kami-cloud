@file:Suppress("unused")

package xyz.scootaloo.kami.server.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.*

/**
 * @author flutterdash@qq.com
 * @since 2022/2/23 14:04
 */

interface SysTask : Entity<SysTask> {

    var id: Int
    var type: Int
    var user: Int
    var created: Long
    var state: Int
    var payload: String

    companion object : Entity.Factory<SysTask>() {
        const val RUNNING = 1
        const val FAILURE = 2
        const val FINISH = 3
    }
}

object SysTasks : Table<SysTask>("tasks") {

    val id = int("id").bindTo { it.id }.primaryKey()
    val type = int("type").bindTo { it.type }
    val user = int("user").bindTo { it.user }
    val state = int("state").bindTo { it.state }
    val created = long("created").bindTo { it.created }
    val payload = varchar("payload").bindTo { it.payload }

}

val Database.tasks get() = sequenceOf(SysTasks)