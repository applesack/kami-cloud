package xyz.scootaloo.kami.server.service

import io.vertx.core.http.HttpServerRequest

/**
 * @author flutterdash@qq.com
 * @since 2022/2/5 10:30
 */
object HttpMessageHelper {

    fun getIpAddress(httpRequest: HttpServerRequest): String {
        return httpRequest.remoteAddress().hostAddress()
    }

}