package xyz.scootaloo.kami.server.controller.dto

import xyz.scootaloo.kami.server.standard.JsonContentIncompleteException
import java.lang.reflect.InvocationTargetException
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

/**
 * @author flutterdash@qq.com
 * @since 2022/2/7 0:44
 */
interface BaseDTO : java.io.Serializable

@Throws(JsonContentIncompleteException::class)
fun <T : BaseDTO> T.checkPropertiesIntegrity(): T {
    this::class.memberProperties.map { prop ->
        try {
            prop.isAccessible = true
            val value = prop.call(this)
            if (value != null && value is BaseDTO) {
                value.checkPropertiesIntegrity()
            }
        } catch (e: UninitializedPropertyAccessException) {
            throw JsonContentIncompleteException()
        } catch (e: InvocationTargetException) {
            throw JsonContentIncompleteException()
        } catch (e: JsonContentIncompleteException) {
          throw e
        } catch (ignore: Throwable) {}
    }
    return this
}