package xyz.scootaloo.kami.server.service

import xyz.scootaloo.kami.server.standard.Type

/**
 * @author flutterdash@qq.com
 * @since 2022/1/13 9:48
 */
object Constant {

    const val HTTP_CONTENT_TYPE = "content-type"
    const val HTTP_AUTHORIZATION = "Authorization"
    const val HTTP_JSON_CONTENT = "application/json;utf-8"

    @Type(String::class) const val ID = "id"
    @Type(String::class) const val USERNAME = "username"
    @Type(Int::class) const val ROLE = "role"

    const val CURRENT_CONFIGURATION = "current-config-source"

    const val HOME_DIR = "home"
    const val CACHE_DIR = "cacheDir"
    const val REPOSITORIES = "repositories"

    const val HOME_KEY = "home"
    const val CACHE_KEY = "cache"
    const val ASSETS_KEY = "assets"

    const val PORT = "port"
    const val PATH = "path"
    const val NAME = "name"
    const val GUEST = "guest"

    const val DEFAULT_HOME_DIR = "./.home/"
    const val DEFAULT_CACHE_DIR = "./cache/"

}