@file:Suppress("unused")

package xyz.scootaloo.kami.server.model

import org.ktorm.database.Database
import org.ktorm.entity.Entity
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.long
import org.ktorm.schema.varchar
import xyz.scootaloo.kami.server.standard.currentTimeMillis

/**
 * @author flutterdash@qq.com
 * @since 2022/2/22 19:43
 */
interface SysLimit : Entity<SysLimit> {

    var id: Int
    var created: Long
    var updated: Long
    var path: String
    var capacity: Long

    companion object : Entity.Factory<SysLimit>() {
        fun create(path: String, limit: Long): SysLimit {
            return SysLimit {
                created = currentTimeMillis()
                updated = currentTimeMillis()
                this.path = path
                this.capacity = limit
            }
        }
    }
}

object SysLimits : Table<SysLimit>("limits") {

    val id = int("id").bindTo { it.id }.primaryKey()
    val created = long("created").bindTo { it.created }
    val updated = long("updated").bindTo { it.updated }
    val path = varchar("path").bindTo { it.path }
    val capacity = long("capacity").bindTo { it.capacity }

}

val Database.limits get() = this.sequenceOf(SysLimits)