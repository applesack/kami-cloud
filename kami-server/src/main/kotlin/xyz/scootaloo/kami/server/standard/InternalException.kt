@file:Suppress("unused")

package xyz.scootaloo.kami.server.standard

import xyz.scootaloo.kami.server.model.AccessType
import xyz.scootaloo.kami.server.model.CreateIllegalLimitReporter
import xyz.scootaloo.kami.server.model.UploadExceedLimitReporter
import xyz.scootaloo.kami.server.service.Constant.CURRENT_CONFIGURATION
import xyz.scootaloo.kami.server.verticle.vertx

/**
 *
 * @author flutterdash@qq.com
 * @since 2021/12/31 16:41
 */

private typealias EL = ExceptionLevel

enum class ExceptionLevel {
    IGNORE, INFO, ERROR
}

open class ServerInternalException : RuntimeException {
    val level: ExceptionLevel

    constructor(msg: String, level: ExceptionLevel) : this(msg, null, true, level)
    constructor(msg: String, cause: Throwable, level: ExceptionLevel) : this(msg, cause, true, level)
    constructor(cause: Throwable, level: ExceptionLevel) : this(null, cause, true, level)

    constructor(
        msg: String, noStackTrace: Boolean, level: ExceptionLevel
    ) : this(msg, null, noStackTrace, level)

    constructor(
        cause: Throwable, noStackTrace: Boolean, level: ExceptionLevel
    ) : this(null, cause, noStackTrace, level)

    constructor(
        msg: String?, cause: Throwable?,
        noStackTrace: Boolean, level: EL
    ) : super(msg, cause, !noStackTrace, !noStackTrace) {
        this.level = level
    }
}

class ConfigItemMissingException(item: String) : ServerInternalException(item.buildMessage(), false, EL.INFO) {
    companion object {
        private fun String.buildMessage(): String {
            val configItem = this
            val configFile = vertx.orCreateContext.get<String>(CURRENT_CONFIGURATION)
            return "配置文件:`${configFile}`, 配置项:`${configItem}`缺失"
        }
    }
}

class VerticleDeployException(msg: String): ServerInternalException(msg, false, EL.INFO)

class CreateLinkException(filepath: String): ServerInternalException("目录路径`${filepath}`不存在", EL.INFO)

class CacheInvalidException(key: String? = null) : ServerInternalException("键失效:`$key`", false, EL.INFO)

class JsonContentIncompleteException : ServerInternalException("", EL.IGNORE)

class UserNotExistsException: ServerInternalException("", EL.IGNORE)

class UserExistsException : ServerInternalException("", EL.IGNORE)

class UserPasswordMistakeException : ServerInternalException("", EL.IGNORE)

class AccessDeniedException(val type: AccessType) : ServerInternalException("", EL.IGNORE)

class AccessViolationException(val path: String): ServerInternalException("路径访问越界: `$path`", EL.INFO)

class UploadExceedLimitException(val reporter: UploadExceedLimitReporter) : ServerInternalException(
    "上传文件大小超过限制, 在路径`${reporter.endpoint}`", EL.INFO
)

class CreateIllegalLimitException(val report: CreateIllegalLimitReporter) : ServerInternalException(
    "", EL.INFO
)

class RequestFormDataIncompleteException : ServerInternalException("表单信息不完整", EL.INFO)

class UploadTaskNotExistsException(taskId: String) : ServerInternalException(
    "文件上传任务不存在: `$taskId`", EL.INFO
)

class FileNotInTaskException(filename: String): ServerInternalException(
    "文件上传任务中不存在这样的文件: `$filename`", EL.INFO
)

class UploadTaskInvalidFormatException(cause: String) : ServerInternalException(
    "文件上传格式错误: $cause", EL.INFO
)