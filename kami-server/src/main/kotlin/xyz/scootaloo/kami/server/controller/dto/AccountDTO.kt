package xyz.scootaloo.kami.server.controller.dto

/**
 * @author flutterdash@qq.com
 * @since 2022/2/1 21:27
 */

/**
 * @property username 用户名
 * @property password 用户密码, 这个密码是密文(且是被公钥加密的)
 * @property appId 标识客户端的一个键, 服务端可以根据这个属性找到解密用户密文的私钥
 * @property token 用户注册或登录时提交
 */
class AccountForm : BaseDTO {
    lateinit var username: String
    lateinit var password: String
    lateinit var appId: String
    var token: String? = null
}