package xyz.scootaloo.kami.server.service

/**
 * 服务的生命周期
 *
 * @author flutterdash@qq.com
 * @since 2022/2/8 15:37
 */

interface ApplicationStageListener {
    fun beforeVerticleDeploy() {}
    fun afterConfigReady(config: AppConfig) {}
    fun afterDatabaseAvailable() {}
    fun afterApplicationStarted() {}
}

object StageEventEmitter {
    private val services by lazy {
        ApplicationServicesStore.services.filterIsInstance<ApplicationStageListener>()
    }

    fun beforeVerticleDeploy() = services.forEach { it.beforeVerticleDeploy() }
    fun afterConfigReady(config: AppConfig) = services.forEach { it.afterConfigReady(config) }
    fun afterDatabaseAvailable() = services.forEach { it.afterDatabaseAvailable() }
    fun afterApplicationStarted() = services.forEach { it.afterApplicationStarted() }
}

object ApplicationServicesStore {
    val services: List<Any> by lazy { retServices() }
    private fun retServices() = listOf(
        CacheService(),
        FileSystemViewService(),
        JwtService(),
        RuleSystemService(),
        TokenService(),
        FileResolver(),
        CrontabService(),
        UploadService(),
        SystemService(),
        FileLogService(),
        BroadcastService()
    )
}