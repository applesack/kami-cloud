package xyz.scootaloo.kami.server.service

import io.vertx.core.Future
import io.vertx.ext.web.RoutingContext
import xyz.scootaloo.kami.server.model.AccessToken
import xyz.scootaloo.kami.server.service.impl.InternalDownloadServiceImpl

/**
 * 提供文件下载相关的功能, 如下载和生成下载链接
 *
 * 只支持单个文件的下载, 默认提供分段下载的支持
 *
 * @author flutterdash@qq.com
 * @since 2022/3/28 9:50
 */
interface DownloadService {

    companion object {
        operator fun invoke(): DownloadService {
            return InternalDownloadServiceImpl
        }
    }

    /**
     * 根据客户端的访问等级和IP地址, 生成一个目标文件的下载链接(只包含uri部分),
     * 类似于 /download/abc.txt?t=just123
     *
     * - 不同的文件下载链接不同, 不同的用户下载同一文件时下载链接不同
     * - 下载链接的有效期为6个小时, 下载
     * - 下载链接仅当前用户可用, 当链接被分发给其他人时, 其他人并不一定可以通过链接下载
     */
    fun createDownloadLink(token: AccessToken, ipAddress: String): Future<String>

    fun download(ctx: RoutingContext, filename: String, ipAddress: String, token: String?): Future<Unit>

    data class DownloadTask(
        val filename: String,
        val userLevel: Int,
        val ipAddress: String
    )
}