package xyz.scootaloo.kami.server.service.broadcast

import xyz.scootaloo.kami.server.model.EntityFile

/**
 * @author flutterdash@qq.com
 * @since 2022/3/12 14:08
 */

fun createFileAddEvent(relativePath: String, filename: String, file: EntityFile): OutboundEvent<FileAddEvent> {
    return OutboundEvent.create(BroadcastEventType.FILE_ADD, FileAddEvent(relativePath, filename, file))
}

fun createFileDeleteEvent(relativePath: String, filename: String): OutboundEvent<FileDeleteEvent> {
    return OutboundEvent.create(BroadcastEventType.FILE_DEL, FileDeleteEvent(relativePath, filename))
}

fun createFileRenameEvent(
    relativePath: String, oldFilename: String, newFilename: String
): OutboundEvent<FileRenameEvent> = OutboundEvent.create(
    BroadcastEventType.FILE_RENAME, FileRenameEvent(relativePath, oldFilename, newFilename)
)

fun createDirCapChangeEvent(
    relativePath: String, limit: Long, free: Long
): OutboundEvent<DirectoryCapacityChangeEvent> = OutboundEvent.create(
    BroadcastEventType.FILE_CAP, DirectoryCapacityChangeEvent(relativePath, limit, free)
)

/**
 * 文件增加
 *
 * 文件拷贝事件: ~
 * 文件创建事件: 新建空文件, 新建空目录
 * 文件移动事件: 一个文件从源路径移动到目标路径(目标路径上触发文件增加事件)
 */
data class FileAddEvent(
    override val path: String, val filename: String, val file: EntityFile
) : FileModifyEventPayload(path)

/**
 * 文件删除
 *
 * 文件删除事件: 一个文件或文件夹在路径上被删除
 * 文件移动事件: 文件从源路径上被删除
 */
data class FileDeleteEvent(override val path: String, val filename: String) : FileModifyEventPayload(path)

/**
 * 文件重命名事件
 *
 * 在一个路径上, 一个文件或文件夹的名称由[oldName]修改为[newName]
 */
data class FileRenameEvent(
    override val path: String, private val oldName: String, private val newName: String
) : FileModifyEventPayload(path)

/**
 * 目录容量变化事件
 *
 * 在一个路径上, 有新的文件被创建或删除, 导致容量发生变化
 */
data class DirectoryCapacityChangeEvent(
    override val path: String, val limit: Long, val free: Long
) : FileModifyEventPayload(path)