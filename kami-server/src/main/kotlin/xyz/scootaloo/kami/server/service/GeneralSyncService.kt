package xyz.scootaloo.kami.server.service

import io.vertx.core.Future
import xyz.scootaloo.kami.server.service.impl.InternalGeneralSyncServiceImpl

/**
 * 通用的同步任务模板
 *
 * @author flutterdash@qq.com
 * @since 2022/3/25 13:26
 */
interface GeneralSyncService {

    fun submit(t: TopicTask): Future<Unit>

    companion object {
        operator fun invoke(): GeneralSyncService {
            return InternalGeneralSyncServiceImpl
        }
    }

    interface TopicTask : Runnable {
        val topic: String
    }
}