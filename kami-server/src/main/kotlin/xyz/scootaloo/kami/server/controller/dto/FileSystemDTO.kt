package xyz.scootaloo.kami.server.controller.dto

/**
 * @author flutterdash@qq.com
 * @since 2022/2/13 10:26
 */

class SimpleFileDTO : BaseDTO {
    lateinit var path: String
    lateinit var filename: String
}

class MoveFileDTO: BaseDTO {
    lateinit var path: String
    lateinit var filename: String
    lateinit var dest: String
}

class RenameFileDTO : BaseDTO {
    lateinit var path: String
    lateinit var filename: String
    lateinit var newName: String
}